# Sirus Service TypeScript

New Sirus+ service API with TypeScript Languange

## Initialization

"->npm install" to install all required dependencies

## Start

"->npm start" to start the server

---

## Changelog

### Tuesday - 19/09/2017 - 10:30

* Intial Commit

### Wednesday - 20/09/2017 - 20:30

* Kid Review
    - New Model "KJAnak" added
    - New queries "kidReview" and "kidReviewByMedrec(no_medrec)" has been addded
    - New Mutation "addKidReview(all field)" has been added

### Friday - 22/09/2017 - 17:00

* Integrated Note
    - New Query "ctByDate(tgl_ct)" has been added
* Examine
    - New Queries "examineByDate(startDate,endDate)" and "examineJoinByVisit(kunjungan)" has been added

### Tuesday - 26/09/2017 - 10:45

* Added new feature SMS Gateway
* Resolver and Schema fixing

### Tuesday - 26/09/2017 - 16:00

* Inform Consent
    - New model "Inform Consent" added
    - New Queries "informConsentAll" and "informConsentPagination(offset, limit)" has been added
    - New Mutations "addInformConsentInfo", "addInformConsentApproval" and "addTaggingOperation" has been added

### Tuesday - 26/09/2017 - 19:15

* Inform Consent
    - Minor bugfix on "informConsentPagination" query
* Consultaion
    - New model "Consultation" has been added
    - New Queries "consultationAll", "consultationPagination(offset, limit)", "consultationByNodok(nodok)", and "consultationByTujuan(nodok_tujuan)" has been added
    - New Mutations "addConsultation" and "updateConsultationAnswer" has been added

### Thursday - 28/09/2017 - 13:30

* Examination
    - bugfix
* Review
    - New mutation added "updateReview"
* Kid Review
    - New mutation added "updateKidReview"

### Thursday - 28/09/2017 - 18:00

* Review
    - Minor bugfix on mutation "updateReview"
* Kid Review
    - Minor bugfix on mutation "updateKidReview"

### Monday - 02/10/2017 - 15:00

* Doctor
    - New Query added "doctorBySpecialist(specialis)"
* ICDX
    - New Query added "checkICD10(icd)"
* Examine
    - New Query added "examineAllByNodok(offset, limit)"
    - New Mutation added "updateDiagnosisExamination"

### Friday - 06/10/2017 - 18:00

* Review
    - New field added "flagKaji"
    - New Query added "reviewByMedrec0(no_medrec)"
    - New Mutation added "reviewFlagUpdate(no_medrec, flagKaji)"
* Kid Review
    - New field added "flagKaji"
    - New Query added "kidReviewByMedrec0(no_medrec)"
    - New Mutation added "kidReviewFlagUpdate(no_medrec, flagKaji)"
* ICDX -> Integrated Note
    - Query "CheckICD10" on ICDX has been moved to Integrated Note
* Graphql YI pm2 optimization
* All sequelize "DATE" data type has been changed to "DATEONLY"

### Monday - 9/10/2017 - 15:30

* Integrated Note
    - Query "checkICD10(a_icdx)" has parameter change, as "checkICD10(no_medrec, a_icdx)"

### Tuesday - 10/10/2017 - 14:00

* Examination
    - Mutation "updateDiagnosisExamination(no_medrec, diagnosa, kasus)" has been through parameter change as "updateDiagnosisExamination(no_periksa, diagnosa, kasus)"
* Integrated Note
    - Queries "ctByMedrec" and "ctByDate" now using "findAll" instead of "findOne"

### Wednesday - 11/10/2017 - 01:00

* Examination
    - Query "examineJoinByVisit(kunjungan)" has been fixed and redefined as "examineJoinByVisit(kunjungan, offset, limit)"
* Integrated Note
    - New query added "ctById(id_ct)"

### Wednesday - 11/10/2017 - 15:15

* Examination
    - Query "examineByDate(startDate, endDate)" has been fixed and redefined as "examineJoinByDateRange(startDate, endDate)"

### Wednesday - 11/10/2017 - 22:50

* Medical Record
    - Few fix for mutation "addMedrec"
* Queue
    - Mutation "addQueue", now affecting Medical Record Table
* Integrated Note
    - New field "no_periksa" added into models
    - New queries "ctByExamine(no_periksa)" and "ctByMedrec1(no_medrec)" has been added
    - Query "ctByMedrec" now has descending sort order

### Thursday - 12/10/2017 - 17:30

* Doctor
    - New query added "doctorByName(nama_dok, offser, limit)"

### Friday - 13/10/2017 - 20:30

* Consultation
    - New Queries "consultationByNodok1(nodok)" and "consultationByTujuan1(nodok_tujuan)" has been added

### Monday - 16/10/2017 - 11:00

* Integrated Note
    - New query added "ctByNodok1(nodok)"

### Tuesday - 17/10/2017 - 20:00

* Inform Consent
    - New field "flag" added to model
    - Queries and Mutations have been redefined in response to recent change

### Thursday - 19/10/2017 - 14:15

* Inform Consent
    - New query "informConsent0(no_inform)" has been added
    - New Mutation "deleteInformConsent(no_infom)" has been added

### Friday - 20/10/2017 - 15:00

* Examination
    - New Query "examineJoinByNo(no_periksa)" has been added
* New excel export function for Report has been added
* New nodemailer function for Request Edit has been added

### Monday - 23/10/2017

* Email gateway function has been added
* Consultatio
    - Query "consultationByNodok1" has been fixed

### Thursday - 9/11/2017 

* All service API has been migrated to new DB with better data structure
* Some fix following new data structure

### Friday - 10/11/2017

* Several fix for new DB applied

### Tuesday - 14/11/2017 

* Some minor bugfix on several model for new DB structure
* Minor bugfix on joinMonsterAdapter following new DB Structure

### Wednesday - 15/11/2017

* Informed Consent
    - Some fix on Informed Consent model
    - Several bugfix on informed consent schema and resolver

### Thursday - 16/11/2017

* Change Review Field's coloumn size in connector
* Add parameter in review, adultReview and updateReview

### Friday - 17/11/2017

* Examine
    - New field added "diagnosa_stroke"
* Informed Consent
    - Several minor fix on informed consent model, schema and resolver

### Monday - 20/11/2017

* Rujukan
    - New model "Rujukan" has been added
    - Several queries and mutation for model rujukan has been added

### Tuesday - 21/11/2017

* Examination
    - Added new query "examineLast1" to get data with flag = 1

### Wednesday - 22/11/2017

* Some bugfix for query "examineLast1"

### Tuesday - 12/12/2017

* Fix some router issue regarding new DB

### Wednesday - 20/12/2017

* Package update

### Thursday - 28/12/2017 - 13:00 - API Service 0.3.0.0

* API service update from 0.2.0.# to 0.3.0.#
* Added Subscription function on API Service (WIP)

### Thursday - 28/12/2017 - 14:30 - API Service 0.3.0.1

* Files "server.ts" has been simplified, now server using 1 server instead of 2
* API service port changed to :6363

### Thursday - 28/12/2017 - 18:30 - API Service 0.3.0.2

* Some bugfix on ticker function

### Friday - 29/12/2017 - 16:30 - API Service 0.3.0.3

* Some fix for subscription
* Added channel on tick subscription

### Friday - 29/12/2017 - 17:30 - API Service 0.3.0.4

* GraphQL Subscription is now fully implemented and working

### Tuesday - 2/1/2018 - 16:45 - API Service 0.3.1.5

* New Subscription "newData" on Examine Model
* Backend source code is now well documented using comment in each section of code
* Files
    - Since file upload operation is no longer needed, files model has been deleted

### Wednesday - 3/1/2018 - 12:00 - API Service 0.3.1.6

* Examination
    - New pubsub on mutation "addExamination"
    - Query "migrateQueueIntoExamination" has been redefined, now it doesn't destroy the data on queue

### Wednesday - 3/1/2018 - 15:00 - API Service 0.3.7-beta

* Versioning now use semver 2.0 instead of 1.0
* API now use separate config files for easy environment adaptation

### Thursday - 4/1/2018 - 14:15 - API Service 0.3.8-beta

* Added argument "nodok" for both dataExchange subscription to avoid data criss-crossing

### Thursday - 4/1/2018 - 14:20 - API Service 0.3.9-Alpha

* Added localhost address config

### Friday - 5/1/2018 - 18:10 - API Service 0.3.10-Beta

* Added new arguments on examineDataExchange Subscriptions

### Thursday - 11/1/2018 - 19:00 - API Service 0.3.11-Beta

* Karyawan Model
    - Added new model "Karyawan"
    - Added new query "karyawanByNIK(nik)"

### Friday - 12/1/2018 - 15:00 - API Service 0.3.12-Beta

* Informed Consent Model
    - Model has been redefined

### Friday - 12/1/2018 - 16:00 - API Service 0.3.13-Beta

* Fix some bug in employee connector
* Redefine review models

### Monday - 15/1/2018 - 17:00 - API Service 0.3.14-Beta

* Added field "tt_perawat" on Review's mutation "updateReview"
* Added column penyakit on Integrated Note

### Thursday - 18/1/2018 - 22:15 - API Service 0.3.15-Beta

* Review   
    - Added new columns "namadok", "nodok", "nama_prw" and "nik"
* Queue
    - New query added "queueAllToday"    
* Informed Consent
    - Mutation "addInformConsentAnastesi" has been redefined

### Friday - 19/1/2018 - 14:45 - API Service 0.3.16-Beta

* Queue
    - Queue Model has been redefined, added new column "tgl_antri"
    - Queue "queueAllToday" has been fixed
    - Mutation "addQueue" and "deleteQueue" has been fixed

### Monday - 22/1/2018 - 14:30 - API Service 0.3.17-Beta

* Queue
    - Query "queueAllToday" has been redefined as "queueAllToday(nodok)"

### Wednesday - 24/1/2018 - 18:30 - API Service 0.3.18-Beta

* Integrated Note
    - New Query added "ctByMedrecTgl(medrec, tgl_ct)"

### Monday - 29/1/2018 - 16:15 - API Service 0.3.19-Beta

*  Midwifery 
    - Added new model "Midwifery"
    - 3 New Queries added to model "MWReview", "MWReviewByMedrec" and "MWReviewByMedrec0"

### Wednesday - 31/1/2018 - 17:25 - API Service 0.3.20-Beta

* Lab Integration
    - Added new Model "LabIntegration"
    - Added 3 new Queries "labIntegrationById(no_medrec)", "labIntegrationAll" and "labIntegrationByDate(tgl_permintaan)"
    - Added 3 new Mutation "addLab", "addRadio" and "addFisio"
* Master Lab
    - Added new Model "MasterLab"
    - Added new Query "masterLabAll"
* Master Radiologi
    - Added new Model "MasterRadiologi"
    - Added new Query "masterRadiologiAll"
* Midwifery
    - Added new Mutation "addMWReview"
* Inform Consent Type
    - Added new Query "informTypeByName(tipe_inform)"

### Thursday - 1/2/2018 - 14:15 - API Service 0.3.20-Beta-Hotfix1

* Several bugfix on Master Lab, Master Radiologi and Inform Consent Type

### Thursday - 1/2/2018 - 15:30 - API Service 0.3.21-Beta

* Midwivery
    - Some Structural Changes on Model
    - Fix on "MWReviewByMedrec0"
    - Added new Mutation "updateMWReview"

### Friday - 2/2/2018 - 16:45 - API Service 0.3.22-Beta

* Midwivery
    - Added new Field "id_integrasi" on Midwifery Model

### Monday - 5/2/2018 - 19:30 - API Service 0.3.23-Beta

* Midwivery
    - Fix on model Midwivery
* Lab Integration
    - Fix on schema and resolver for Lab Integration

### Tuesday - 20/2/2018 - 10:30 - API Service 0.3.24-Beta

* Queue
    - Added new query "QueueToday(poli, nama_dok, flag)"
* Subscription
    - Subscription model has been deleted