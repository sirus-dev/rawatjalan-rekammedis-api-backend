FROM node:11.1.0-alpine
COPY . /app
WORKDIR /app 
RUN apk add --no-cache --virtual .build-deps make gcc g++ python \
 && npm install natives -f \
 && apk del .build-deps
EXPOSE 5000
ENTRYPOINT [ "npm", "run"]
CMD [ "start"]