import * as express from 'express';
import * as bodyParser from 'body-parser';
import { createServer } from 'http';
import { graphql, execute, subscribe } from 'graphql';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { PubSub } from 'graphql-subscriptions';
import { makeExecutableSchema } from 'graphql-tools';
import { apolloUploadExpress } from 'apollo-upload-server';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import * as cors from 'cors';
import * as ejs from 'ejs';
import * as joinMonster from 'join-monster';
import * as joinMonsterAdapt from 'join-monster-graphql-tools-adapter';
import { typeDefs } from './api/typeMerger';
import { resolvers } from './api/resolverMerger';
import { adapter } from './api/joinMonsterAdapter';
import config from './config';
const { APP_PORT, LOCALHOST } = config;
var router = express.Router();

// Set port for appplication
// const APP_PORT = 5353;

// Creating Express and defining Public Subscription
const graphQLServer = express();
const pubSub = new PubSub();

// Create Schema from existing Types and Resolvers
const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

// Create Join Monster Query and adapting it with GraphQL Apollo
joinMonsterAdapt(schema, adapter);
// Defining server application
graphQLServer.use(cors());
graphQLServer.use(
  '/graphql',
  bodyParser.json(),
  graphqlExpress({ context: { pubSub }, schema: schema })
);
graphQLServer.use(
  '/graphiql',
  graphiqlExpress({
    endpointURL: '/graphql',
    subscriptionsEndpoint: `ws://${LOCALHOST}:${APP_PORT}/subscriptions`
  })
);


graphQLServer.use('/', function (req: any, res: any) {
  res.json({ message: 'Hello, Welcome to SIRUS Rawat Jalan Rekam Medis API' });
});

// Routing Server
require('./router/sms.js')(graphQLServer, router);
require('./router/laporan.js')(graphQLServer, router);
require('./router/request.js')(graphQLServer, router, ejs);
require('./router/ctapi.js')(graphQLServer, router);

// Creating Server
const APIServer = createServer(graphQLServer);

// Creating server listener
APIServer.listen(APP_PORT, () => {
  console.log(`App is running on http://${LOCALHOST}:${APP_PORT}/graphiql`);

  // Creating GraphQL Subscription Server using WebSocket
  const subscriptionServer = SubscriptionServer.create(
    {
      schema,
      execute,
      subscribe
    },
    {
      server: APIServer,
      path: '/subscriptions'
    }
  );
});
