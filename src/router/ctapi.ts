import * as mysql from 'mysql';
import config from '../config';
const { MYSQL_DB, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_HOST } = config;

module.exports = function (app: any, router: any) {

    router.get('/patient/:name', function(req: any, res: any) {

        var connection = mysql.createPool({
            host: MYSQL_HOST,
            user: MYSQL_USERNAME,
            password: MYSQL_PASSWORD,
            database: MYSQL_DB
        });

        connection.query('SELECT * FROM ct_pasien WHERE nama_pasien LIKE ?', '%' + [req.params.name] + '%',
        function (error: any, results: any, fields: any) {
            if (error) {
                throw error;
            }
            res.send(JSON.stringify({'status': 200, 'response': results}, null, 2));
        });

    });

    app.use('/ct', router);

};