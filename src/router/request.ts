import * as nodemailer from 'nodemailer';
import * as mysql from 'mysql';
import config from '../config';
const { MYSQL_DB, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_HOST } = config;
var path = require('path');
module.exports = function (app: any, router: any, ejs: any) {

    // Create route
    router.get('/request/:no&:alasan&:tgl', function (req: any, res: any) {
      console.log(req.params.no + ' & ' + req.params.alasan + ' & ' + req.params.tgl);
      // Create connection to DB
      var connection = mysql.createPool({
        host: MYSQL_HOST,
        user: MYSQL_USERNAME,
        password: MYSQL_PASSWORD,
        database: MYSQL_DB
      });

      // Calling query and fetch data
      connection.query(
          `SELECT
          logperiksa.no_periksa,
          dtdokter.nodok,
          dtdokter.nama_dok,
          dtdokter.specialis,
          dtpasien.no_medrec,
          dtpasien.nama,
          dtpasien.jln1,
          dtpasien.no,
          dtpasien.rt,
          dtpasien.rw,
          dtpasien.desa,
          dtpasien.kecamatan,
          dtpasien.kota
        FROM logperiksa
        LEFT JOIN dtdokter ON logperiksa.nodok = dtdokter.nodok
        LEFT JOIN dtpasien ON logperiksa.no_medrec = dtpasien.no_medrec
        WHERE logperiksa.no_periksa = ?;`, [req.params.no],
      function(err: any, rows: any, fields: any) {
        console.log(rows[0].nodok);
        nodemailer.createTestAccount((err, account) => {
            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
              host: 'smtp.gmail.com',
              port: 465,
              secure: true, // true for 465, false for other ports
              auth: {
                user: 'sirusrsmary@gmail.com', // generated ethereal user
                pass: 'sirusassul2017'  // generated ethereal password
              }
            });

            // setup email data with unicode symbols
            let mailOptions = {
              from: '"Rekam Medis Sirus RS Mary Cileungsi 🚑" <sirusmarycileungsi@sirus.com>', // sender address
              to: 'rio@lussa.net', // list of receivers
              subject: 'Permintaan Edit Data Rekam Medis', // Subject line
              // Email content written in HTML
              html:
              `<table width="100%" bgcolor="#eaeaea">
              <tr>
                  <td align="center" bgcolor="#314961" style="padding: 20px;">
                      <h1 style="color:white;">Surat Permintaan Suntingan</h1>
                  </td>
              </tr>
              <tr>
                  <td align="center">
                      <table b
                      gcolor="white" width="90%" style="padding: 10px;">
                          <tr>
                              <td>
                              <p>Kepada :</p>
                              <p style="margin-top: -10px">Yth. Manajemen</p>
                              <p style="margin-top: -10px">${req.params.tgl}</p>
                              <p>&nbsp</p>
                              <p>Yang dibawah ini :</p>
                              <p style="margin-top: -10px">Dokter : ${rows[0].nama_dok}</p>
                              <p style="margin-top: -10px">No. Dokter : ${rows[0].nodok}</p>
                              <p style="margin-top: -10px">Spesialis : ${rows[0].specialis}</p>
                              <p>&nbsp</p>
                              <p>Menyatakan permohonan untuk menyunting data riwayat pasien atas :</p>
                              <p style="margin-top: -10px">Nama : ${rows[0].nama}</p>
                              <p style="margin-top: -10px">No. RM : ${rows[0].no_medrec}</p>
                              <p style="margin-top: -10px">Alamat : ${rows[0].jln1 + ' ' + rows[0].no + ' ' +
                                rows[0].rt + ' ' + rows[0].rw + ' ' + rows[0].desa + ' ' + rows[0].kecamatan +
                                ' ' + rows[0].kota}</p>
                              <p>&nbsp</p>
                              <p style="margin-top: -10px">Dengan Alasan : </p>
                              <p style="margin-top: -10px">${req.params.alasan}</p>
                              </td>
                          </tr>
                          <tr>
                              <td><button style="border:0px;background-color:#54a4e7;color:white;
                                padding:10px;font-weight:bold;border-radius:5px; margin:30px 0px 10px 0px;">Sunting</button>
                              <button style="border:0px;background-color:#ff5050;color:white;
                                padding:10px;font-weight:bold;border-radius:5px; margin:30px 0px 10px 0px;">Tolak</button></td>
                          </tr>
                      </table>
                  </td>
              </tr>
          </table>` // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                return console.log(error);
              }
              console.log('Message sent: %s', info.messageId);
              res.json({
                success: true,
                message: 'Message sent: %s ' + info.messageId,
              });
            });

          });

        });
      });

app.use('/edit', router);

};