import * as excel from 'exceljs';
import * as tempfile from 'tempfile';
import * as mysql from 'mysql';
import config from '../config';
const { MYSQL_DB, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_HOST } = config;
// Export routed path
module.exports = function (app: any, router: any) {

    router.get('/download', function(req: any, res: any) {
        // Creating connection to DB
        var connection = mysql.createPool({
            host: MYSQL_HOST,
            user: MYSQL_USERNAME,
            password: MYSQL_PASSWORD,
            database: MYSQL_DB
        });

        // Calling query and fetching data from DB
        connection.query(
            `SELECT
            logperiksa.tgl,
            dtdokter.nodok,
            dtdokter.nama_dok,
            dtdokter.specialis,
            dtpasien.no_medrec,
            dtpasien.nama,
            dtpasien.jln1,
            dtpasien.no,
            dtpasien.rt,
            dtpasien.rw,
            dtpasien.desa,
            dtpasien.kecamatan,
            dtpasien.kota,
            logperiksa.diagnosa,
            logperiksa.kasus,
            logperiksa.jaminan,
            logperiksa.kunjungan,
            logperiksa.umur,
            logperiksa.kelamin
        FROM logperiksa
        LEFT JOIN dtdokter ON logperiksa.nodok = dtdokter.nodok
        LEFT JOIN dtpasien ON logperiksa.no_medrec = dtpasien.no_medrec`,
        function(err: any, rows: any, fields: any) {
            try {
                // Create workbook for data export
                var workbook = new excel.Workbook();
                workbook.creator = 'Sirus';
                var worksheet = workbook.addWorksheet('Laporan');

                // Assigning workbook column to fethced data column
                worksheet.columns = [
                    { header: 'No', key: 'no', width: 5},
                    { header: 'Tanggal', key: 'tgl', width: 11},
                    { header: 'Nama Pasien', key: 'nama', width: 64},
                    { header: 'No RM', key: 'no_medrec', width: 7},
                    { header: 'Jenis Kelamin', key: 'kelamin', width: 13},
                    { header: 'Umur', key: 'umur', width: 6},
                    { header: 'Dokter', key: 'nama_dokter', width: 64},
                    { header: 'Spesialis', key: 'specialist', width: 32},
                    { header: 'Pembayaran', key: 'pembayaran', width: 12},
                    { header: 'Diagnosa', key: 'diagnosa', width: 20},
                    { header: 'Kasus', key: 'kasus', width: 7},
                    { header: 'Kunjungan', key: 'kunjungan', width: 10},
                    { header: 'Alamat', key: 'alamat', width: 128}
                ];
                for (let i = 0; i < rows.length; i++) {
                    worksheet.addRow({no: i + 1, tgl: rows[i].tgl, nama: rows[i].nama,
                        no_medrec: rows[i].no_medrec, kelamin: rows[i].kelamin,
                        umur: rows[i].umur, nama_dokter: rows[i].nama_dok, specialist: rows[i].specialis,
                        pembayaran: rows[i].jaminan, diagnosa: rows[i].diagnosa, kasus: rows[i].kasus,
                        kunjungan: rows[i].kunjungan, alamat: rows[i].jln1 + ' ' + rows[i].no + ' ' +
                            rows[i].rt + ' ' + rows[i].rw + ' ' + rows[i].desa + ' ' + rows[i].kecamatan + ' ' + rows[i].kota});
                }

                // Exporting data
                var tempFilePath = tempfile('Laporan.xlsx');
                workbook.xlsx.writeFile(tempFilePath).then(function() {
                    console.log('File is being writen');
                    res.sendFile(tempFilePath, function(err: any) {
                        console.log('-------Error downloading file: ' + err);
                    });
                });
                } catch (err) {
                    console.log('000000 Error: ' + err);
                }
        });
    });

    app.use('/laporan', router);

};