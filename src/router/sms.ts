module.exports = function (app: any, router: any) {

    // Reroute to external sms gateway to send SMS to patient
    router.get('/send/:number&:message', function(req: any, res: any) {
        res.redirect(`https://reguler.zenziva.net/apps/smsapi.php?userkey=b3gv01&passkey=lussa2017
            &nohp=${req.params.number}&pesan=${req.params.message}`);
    });

    app.use('/sms', router);

};