import joinMonster from 'join-monster';
import { db, Report } from '../connectors';

// create the resolve functions for the available GraphQL queries
export const reportResolver = {

    Query: {
        /**
         * reportById
         *
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        reportById(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
               return db.query(sql)
               .spread((results, metadata) => results);
            }, { dialect: 'mysql' });
        },
        /**
         * report
         *
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        report(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
                let query = `${sql} LIMIT ${args.offset}, ${args.limit}`;
                return db.query(query)
                .spread((results, metadata) => results);
            }, { dialect: 'mysql' });
        },
    },
    Mutation: {
        /**
         * addReport
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addReport(_: any, args: any) {
            return Report.create(args);
        }
    }
};