// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const reportType = `
type Doctorr {
  name: String!
  nodok: String!
  nickName: String
  gender: String
  birthday: String
  occupation: String
  company: String
  religion: String
  address: String
  contact: String
  number: String
  specialist: String
  npwp: String
}

type Patientr {
  no_medrec: String
  name: String!
  nickName: String
  birthday: String
  gender: String
  occupation: String
  company: String
  religion: String
  address: String
  contact: String
  insurance: String
}

type Report {
  no_lap: String
  tgl: String
  no_medrec: Patientr
  nodok: Doctorr
  pembayaran: String
  diagnosa: String
  kasus: String
  kunjungan: String
  rujuk: String
  rawat_inap: String
  alamat: String
}

type Query {
  reportById(
    no_lap: String!
  ): Report
  report(
    offset: Int
    limit: Int
  ): [Report]
}

type Mutation {
  addReport(
    no_lap: String!
    tgl: String!
    no_medrec: String!
    nodok: String!
    pembayaran: String
    diagnosa: String
    kasus: String
    kunjungan: String
    rujuk: String
    rawat_inap: String
    alamat: String
  ): Report
}

schema {
  query: Query
  mutation: Mutation
}
`;