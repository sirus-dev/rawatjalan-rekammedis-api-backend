import { MasterLab } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const MasterLabResolver = {

    Query: {
        /**
         * masterLabAll
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        masterLabAll(_: any, args: any) {
            return MasterLab.findAll({where: args});
        },
    },
};