// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const MasterLabType = `
type MasterLab {
  id_lab: Int
  nama_pemeriksaan: String
  kelompok: String
}

type Query {
  masterLabAll: [MasterLab]
}

schema {
  query: Query
}
`;