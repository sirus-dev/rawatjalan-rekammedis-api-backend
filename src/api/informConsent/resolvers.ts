import { InformConsent } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const informConsentResolver = {

    Query: {
        /**
         * informConsentAll
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        informConsentAll(_: any, args: any) {
            return InformConsent.findAll({where: {flag: 0}});
        },
        /**
         * informConsentPagination
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        informConsentPagination(_: any, args: any) {
            return InformConsent.findAll({where: {flag: 0}}, { offset: args.offset, limit: args.limit });
        },
        /**
         * informConsent0
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        informConsent0(_: any, args: any) {
            return InformConsent.findAll({where: {no_medrec: (args.no_medrec), flag: 0}, order: [['tgl_inform', 'DESC']], limit: 1 });
        }
    },
    Mutation: {
        /**
         * addInformConsentInfo
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addInformConsentInfo(_: any, args: any) {
            return InformConsent.create({
                no_inform: (args.no_inform),
                no_medrec: (args.no_medrec),
                pelaksana_tindakan:  (args.pelaksana_tindakan),
                pemberi_info:  (args.pemberi_info),
                penerima_info:  (args.penerima_info),
                info_diag_kerja:  (args.info_diag_kerja),
                checklist_info_diag_kerja: (args.checklist_info_diag_kerja),
                dasar_diag:  (args.dasar_diag),
                checklist_dasar_diag: (args.checklist_dasar_diag),
                tindakan_dokter:  (args.tindakan_dokter),
                checklist_tindakan_dokter: (args.checklist_tindakan_dokter),
                indikasi_tindakan:  (args.indikasi_tindakan),
                checklist_indikasi_tindakan: (args.checklist_indikasi_tindakan),
                tatacara:  (args.tatacara),
                checklist_tatacara: (args.checklist_tatacara),
                tujuan:  (args.tujuan),
                checklist_tujuan: (args.checklist_tujuan),
                risiko:  (args.risiko),
                checklist_risiko: (args.checklist_risiko),
                komplikasi:  (args.komplikasi),
                checklist_komplikasi: (args.checklist_komplikasi),
                prognosis:  (args.prognosis),
                checklist_prognosis: (args.checklist_prognosis),
                alternatif:  (args.alternatif),
                checklist_alternatif: (args.checklist_alternatif),
                lain:  (args.lain),
                checklist_lain: (args.checklist_lain),
                tt_dokter: (args.tt_dokter),
                tt_pasien: (args.tt_pasien),
                tt_saksi1: (args.tt_saksi1),
                tt_saksi2: (args.tt_saksi2),
                flag: 0
            });
        },
        /**
         * addInformConsentAnastesi
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addInformConsentAnastesi(_: any, args: any) {
            return InformConsent.create({
                no_inform: (args.no_inform),
                no_medrec: (args.no_medrec),
                pelaksana_tindakan: (args.pelaksana_tindakan),
                pemberi_info: (args.pemberi_info),
                penerima_info: (args.penerima_info),
                a_u_tindakan_dokter: (args.a_u_tindakan_dokter),
                a_u_tatacara: (args.a_u_tatacara),
                a_u_tujuan: (args.a_u_tujuan),
                a_u_risiko: (args.a_u_risiko),
                a_u_komplikasi: (args.a_u_komplikasi),
                a_r_tindakan_dokter: (args.a_r_tindakan_dokter),
                a_r_tatacara: (args.a_r_tatacara),
                a_r_tujuan: (args.a_r_tujuan),
                a_r_risiko: (args.a_r_risiko),
                a_r_komplikasi: (args.a_r_komplikasi),
                a_r_alternatif: (args.a_r_alternatif),
                a_l_tindakan_dokter: (args.a_l_tindakan_dokter),
                a_l_tatacara: (args.a_l_tatacara),
                a_l_tujuan: (args.a_l_tujuan),
                a_l_risiko: (args.a_l_risiko),
                a_l_komplikasi: (args.a_l_komplikasi),
                a_l_alternatif: (args.a_l_alternatif),
                sedasi_tindakan_dokter: (args.sedasi_tindakan_dokter),
                sedasi_tatacara: (args.sedasi_tatacara),
                sedasi_tujuan: (args.sedasi_tujuan),
                sedasi_risiko: (args.sedasi_risiko),
                sedasi_komplikasi: (args.sedasi_komplikasi),
                sedasi_prognosis: (args.sedasi_prognosis),
                sedasi_lain: (args.sedasi_lain),
                tt_dokter: (args.tt_dokter),
                tt_pasien: (args.tt_pasien),
                tt_saksi1: (args.tt_saksi1),
                tt_saksi2: (args.saksi2),
                flag: 0
            });
        },
        /**
         * addInformConsentApproval
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addInformConsentApproval(_: any, args: any) {
            return InformConsent.update({
                nama_ortu: (args.nama_ortu),
                umur_ortu: (args.umur_ortu),
                kelamin_ortu: (args.kelamin_ortu),
                alamat:  (args.alamat),
                ktp: (args.ktp),
                tgl_inform: (args.tgl_inform),
                jam_inform: (args.jam_inform),
                yg_menyatakan:  (args.yg_menyatakan),
                file_yg_menyatakan:  (args.file_yg_menyatakan),
                dokter_operator:  (args.dokter_operator),
                file_dokter_operator:  (args.file_dokter_operator),
                saksi1:  (args.saksi1),
                file_saksi1:  (args.file_saksi1),
                saksi2:  (args.saksi2),
                file_saksi2:  (args.file_saksi2),
                tindakan_operasi:  (args.tindakan_operasi),
                tipe_tindakan: (args.tipe_tindakan)
            }, { where: {no_inform: (args.no_inform)} });
        },
        /**
         * addTaggingOperation
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addTaggingOperation(_: any, args: any) {
            return InformConsent.update({
                penandaan_operasi:  (args.penandaan_operasi),
            }, { where: {no_inform: (args.no_inform)} });
        },
        /**
         * updateFlagInform
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        updateFlagInform(_: any, args: any) {
            return InformConsent.update({
                flag: (args.flag)
            }, {where: {no_inform: (args.no_inform)}});
        },
        /**
         * deleteInformConsent
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        deleteInformConsent(_: any, args: any) {
            return InformConsent.destroy({where: args});
        },
    },
};