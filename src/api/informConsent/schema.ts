// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const InformConsentType = `
type InformConsent {
  no_inform: String
  no_medrec: String
  nama_ortu: String
  umur_ortu: String
  kelamin_ortu: String
  alamat:  String
  ktp: String
  tgl_inform: String
  jam_inform: Int
  yg_menyatakan:  String
  file_yg_menyatakan:  String
  dokter_operator:  String
  file_dokter_operator:  String
  saksi1:  String
  file_saksi1:  String
  saksi2:  String
  file_saksi2:  String
  tindakan_operasi:  String
  tipe_tindakan: String
  pelaksana_tindakan:  String
  pemberi_info:  String
  penerima_info:  String
  info_diag_kerja:  String
  checklist_info_diag_kerja: String
  dasar_diag:  String
  checklist_dasar_diag: String
  tindakan_dokter:  String
  checklist_tindakan_dokter: String
  indikasi_tindakan:  String
  checklist_indikasi_tindakan: String
  tatacara:  String
  checklist_tatacara: String
  tujuan:  String
  checklist_tujuan: String
  risiko:  String
  checklist_risiko: String
  komplikasi:  String
  checklist_komplikasi: String
  prognosis:  String
  checklist_prognosis: String
  alternatif:  String
  checklist_alternatif: String
  lain:  String
  checklist_lain: String
  penandaan_operasi:  String
  a_u_tindakan_dokter: String
  a_u_tatacara: String
  a_u_tujuan: String
  a_u_risiko: String
  a_u_komplikasi: String
  a_r_tindakan_dokter: String
  a_r_tatacara: String
  a_r_tujuan: String
  a_r_risiko: String
  a_r_komplikasi: String
  a_r_alternatif: String
  a_l_tindakan_dokter: String
  a_l_tatacara: String
  a_l_tujuan: String
  a_l_risiko: String
  a_l_komplikasi: String
  a_l_alternatif: String
  sedasi_tindakan_dokter: String
  sedasi_tatacara: String
  sedasi_tujuan: String
  sedasi_risiko: String
  sedasi_komplikasi: String
  sedasi_prognosis: String
  sedasi_lain: String
  tt_dokter: String
  tt_pasien: String
  tt_saksi1: String
  tt_saksi2: String
  flag: Int
}

type Query {
  informConsentAll: [InformConsent]
  informConsentPagination(
    offset: Int,
    limit: Int
  ): [InformConsent]
  informConsent0(
    no_medrec: String
  ): [InformConsent]
}

type Mutation {
  addInformConsentInfo(
    no_inform: String
    no_medrec: String
    pelaksana_tindakan:  String
    pemberi_info:  String
    penerima_info:  String
    info_diag_kerja:  String
    checklist_info_diag_kerja: String
    dasar_diag:  String
    checklist_dasar_diag: String
    tindakan_dokter:  String
    checklist_tindakan_dokter: String
    indikasi_tindakan:  String
    checklist_indikasi_tindakan: String
    tatacara:  String
    checklist_tatacara: String
    tujuan:  String
    checklist_tujuan: String
    risiko:  String
    checklist_risiko: String
    komplikasi:  String
    checklist_komplikasi: String
    prognosis:  String
    checklist_prognosis: String
    alternatif:  String
    checklist_alternatif: String
    lain:  String
    checklist_lain: String
    tt_dokter: String
    tt_pasien: String
    tt_saksi1: String
    tt_saksi2: String
  ): InformConsent
  addInformConsentAnastesi(
    no_inform: String
    no_medrec: String
    pelaksana_tindakan: String
    pemberi_info: String
    penerima_info: String
    a_u_tindakan_dokter: String
    a_u_tatacara: String
    a_u_tujuan: String
    a_u_risiko: String
    a_u_komplikasi: String
    a_r_tindakan_dokter: String
    a_r_tatacara: String
    a_r_tujuan: String
    a_r_risiko: String
    a_r_komplikasi: String
    a_r_alternatif: String
    a_l_tindakan_dokter: String
    a_l_tatacara: String
    a_l_tujuan: String
    a_l_risiko: String
    a_l_komplikasi: String
    a_l_alternatif: String
    sedasi_tindakan_dokter: String
    sedasi_tatacara: String
    sedasi_tujuan: String
    sedasi_risiko: String
    sedasi_komplikasi: String
    sedasi_prognosis: String
    sedasi_lain: String
    tt_dokter: String
    tt_pasien: String
    tt_saksi1: String
    tt_saksi2: String
  ): InformConsent
  addInformConsentApproval(
    no_inform: String
    nama_ortu: String
    umur_ortu: String
    kelamin_ortu: String
    alamat:  String
    ktp: String
    tgl_inform: String
    jam_inform: Int
    yg_menyatakan:  String
    file_yg_menyatakan:  String
    dokter_operator:  String
    file_dokter_operator:  String
    saksi1:  String
    file_saksi1:  String
    saksi2:  String
    file_saksi2:  String
    tindakan_operasi:  String
    tipe_tindakan: String
  ): InformConsent
  addTaggingOperation(
    no_inform: String
    penandaan_operasi:  String
  ): InformConsent
  updateFlagInform(
    no_inform: String
    flag: Int
  ): InformConsent
  deleteInformConsent(
    no_inform: String
  ): InformConsent
}

schema {
  query: Query
}
`;