import { Doctor } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const doctorResolver = {

    Query: {
        /**
         * Doctors by nodok
         * @param _
         * @param args
         * @returns
         */
        doctorByNodok(_: any, args: any) {
            return Doctor.findOne({where: args});
        },
        /**
         * Doctors
         * @param _
         * @param args
         */
        doctors(_: any, args: any) {
            return Doctor.findAll({offset: args.offset, limit: args.limit});
        },
        /**
         * Doctors by specialist
         * @param _
         * @param args
         * @returns
         */
        doctorBySpecialist(_: any, args: any) {
            return Doctor.findAll({where: {specialis: (args.specialis)}}, {offset: args.offset, limit: args.limit});
        },
        /**
         * Doctor by name
         * @param _
         * @param args
         * @returns
         */
        doctorByName(_: any, args: any) {
            return Doctor.findAll({where: {nama_dok: {$like: '%' + args.nama_dok + '%'}}, offset: args.offset, limit: args.limit});
        },
    },
};