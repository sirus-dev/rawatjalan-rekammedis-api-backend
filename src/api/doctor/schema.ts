// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const doctorType = `
type Doctor {
  nodok: String
  nama_dok: String
  specialis: String
  pers_kons: Float
  pers_tind: Float
  tgl_masuk: String
  by_rujuk: String
  no_urut: String
  nm_panggil: String
  jenkel: String
  tempat: String
  tgl: String
  bln: String
  thn: String
  sts: String
  pekerjaan: String
  pt: String
  kp: String
  jln: String
  no: String
  rt: String
  rw: String
  desa: String
  kec: String
  kota: String
  bank: String
  an: String
  cabang: String
  no_rek: String
  s1: String
  tahun1: String
  s2: String
  tahun2: String
  s3: String
  tahun3: String
  praktek1: String
  praktek2: String
  praktek3: String
  jam: String
  xtgl: String
  userid: String
  id2: String
  no_ktp: String
  nama_panggil: String
  agama: String
  area: String
  telp: String
  areahp1: String
  hp1: String
  areapager: String
  pagerno1: String
  pswtpager: String
  tgls1: String
  blns1: String
  tgls2: String
  blns2: String
  tgls3: String
  blns3: String
  no_sp: String
  no_sip: String
  no_dir: String
  no_sptp: String
  tglsk1: String
  tglsk2: String
  tglsk3: String
  blnsk1: String
  blnsk2: String
  blnsk3: String
  tglsk4: String
  blnsk4: String
  no_npwp: String
  tglnpwp: String
  blnnpwp: String
  thnnpwp: String
  cmbbpk: String
  areahp2: String
  hp2: String
  tahunsk1: String
  tahunsk2: String
  tahunsk3: String
  tahunsk4: String
  garantifee: Int
  transport: Int
  email: String
  status: Int
  tgl_terimanpwp: String
  sts_pegawai: Int
  ahli: Int
  alat_dokter: Int
  alat_rs: Int
  kd_poli: String
  sts_pegawai2: Int
  sts_npwp: Int
  npwp1: String
  npwp2: String
  npwp3: String
  npwp4: String
  npwp5: String
  jabatanmedis: String
  tgl_praktek: String
  sts_npwp2: String
  sts_pegawai3: String
  sts_keahlian: String
}

type Query {
  doctorByNodok(
    nodok: String!
  ): Doctor
  doctors(
    offset: Int!
    limit: Int!
  ): [Doctor]
  doctorBySpecialist(
    specialis: String!
    offset: Int!
    limit: Int!
  ): [Doctor]
  doctorByName(
    nama_dok: String
    offset: Int
    limit: Int
  ): [Doctor]
}

schema {
  query: Query
}
`;