// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const medRecType = `
type MRExamine {
  no_periksa: String
  kodeUnit: String
  poli: String
  tgl: String
  nodok: String
  nama_dok: String
  specialis: String
  no_registrasi: String
  no_medrec: String
  nama_pasien: String
  diagnosa: String
  no_antrian: Int
  kasus: String
  jaminan: String
  kunjungan: String
  rujuk: String
  umur: Int
  tgl_lahir: String
  kelamin: String
  status_pasien: String
  kaji: String
  flag: Int
}

type MRReview {
  no_kaji: String
  no_medrec: String
  keluhan: String
  resikoA1: String
  resikoA2: String
  resikoB1: String
  resikoHasil1: String
  resikoHasil2: String
  resikoHasil3:String
  resikoTindakan1action: String
  resikoTindakan2action: String
  resikoTindakan3action: String
  resikoTindakan1: String
  resikoTindakan2: String
  resikoTindakan3: String
  td: String
  td1: Int
  td2: Int
  nadi: String
  nadi1: Int
  nadi2: Int
  pernafasan: String
  pernafasan1: Int
  pernafasan2: Int
  suhu: Float
  tb: Int
  bb: Int
  nyeri: String
  resiko_jatuh_a: String
  resiko_jatuh_b: String
  gizi1: String
  gizi2: String
  gizi3: String
  gizi3_lainnya: String
  status_fungsional: String
  status_fungsional_ket: String
  anamnesa_keluhan: String
  anamnesa_riwayat_penyakit: String
  fisik_kepala: String
  fisik_leher: String
  fisik_mulut: String
  fisik_paru: String
  fisik_genitalia: String
  fisik_mata: String
  fisik_tht: String
  fisik_jantung: String
  fisik_abdomen: String
  fisik_ekstremitas: String
  status_gizi: String
  pemeriksaan_gigi: String
  pemeriksaan_gigi_ket: String
  status_lokalis: String
  pemeriksaan_penunjang: String
  diagnosis: String
  rencana: String
  dirujuk: String
  dirujuk_lainnya: String
  flagKaji: Int
}

type MedRec {
  no_medrec: String
  pasien: [Patientr]
  kajian: [MRReview]
  periksa: [MRExamine]
}

type Query {
  medRec(
    no_medrec: String!
  ): MedRec
  allMedrec(
    offset: Int
    limit: Int
  ): [MedRec]
}

type Mutation {
  addMedrec(
    no_medrec: String!
  ): MedRec
}

schema {
  query: Query
  mutation: Mutation
}
`;