import joinMonster from 'join-monster';
import { db, RM } from '../connectors';

// create the resolve functions for the available GraphQL queries
export const medRecResolver = {

    Query: {
        /**
         * Med Rec
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        async medRec(parent: any, args: any, ctx: any, resolveInfo: any) {
            await RM.findOrCreate({where: {
                no_medrec: (args.no_medrec),
                pasien: (args.no_medrec),
                kajian: (args.no_medrec),
                periksa: (args.no_medrec)
            }});
            return joinMonster(resolveInfo, ctx, sql => {
               return db.query(sql)
               .spread((results, metadata) => results);
            }, { dialect: 'mysql' });
        },
        /**
         * All Medrec
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        allMedrec(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
                let query = `${sql} LIMIT ${args.offset}, ${args.limit}`;
                return db.query(query)
                .spread((results, metadata) => results);
            }, { dialect: 'mysql' });
        },
    },


    Mutation: {
        /**
         * Add Medrec
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addMedrec(_: any, args: any) {
            return RM.findOrCreate({where: {
                no_medrec: (args.no_medrec),
                pasien: (args.no_medrec),
                kajian: (args.no_medrec),
                periksa: (args.no_medrec)
            }})
            .spread((result, created) => result);
        }
    }
};