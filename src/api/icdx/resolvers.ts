import { ICDX } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const ICDXResolver = {

    Query: {
        /**
         * icdx
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        icdx(_: any, args: any) {
            return ICDX.findAll({where: args});
        },
        /**
         * icdxPagination
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        icdxPagination(_: any, args: any) {
            return ICDX.findAll({where: {
                penyakit: {$like: '%' + args.penyakit + '%'}}, offset: args.offset, limit: args.limit});
        },
        /**
         * icdxByName
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        icdxByName(_: any, args: any) {
            return ICDX.findAll({where: args});
        }
    },
};