// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const ICDXType = `
type ICDX {
  icd: String
  dtd: String
  parent: String
  penyakit: String
}

type Query {
  icdx: [ICDX]
  icdxPagination(
    penyakit: String,
    offset: Int,
    limit: Int
  ): [ICDX]
  icdxByName(
    penyakit: String
  ): [ICDX]
}

schema {
  query: Query
}
`;