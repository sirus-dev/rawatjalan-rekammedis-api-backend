import { Karyawan } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const karyawanResolver = {

    Query: {
        /**
         * Karyawan By NIK
         * @param {*} _
         * @param {*} args
         * @returns
         */
        karyawanByNIK(_: any, args: any) {
            return Karyawan.findOne({where: args});
        },
    },

};