// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const KaryawanType = `
type Karyawan {
  nik: String
  nama: String
  panggilan: String
  jnsktp: String
  ktp: String
  jenkel: String
  tempat: String
  tgllhr: String
  blnlhr: String
  thnlhr: String
  sts: String
  agama: String
  kp: String
  jln: String
  desa: String
  kecamatan: String
  no: String
  kota: String
  rt: String
  rw: String
  areahp1: String
  hp1: String
  areahp2: String
  hp2: String
  area: String
  telp: String
  areapager: String
  pager1: String
  pager2: String
  photo: String
  bpk: String
  tglmsk: String
  blnmsk: String
  thnmsk: String
  tglklr: String
  thnklr: String
  blnklr: String
  kd_bagian: String
  kd_jabatan: String
  shift: String
  status: String
  kd_unit: String
  no_urut: String
  kd_golongan: String
  kd_ptkp: String
  absensi: Int
  flag_keluar: String
  nilai: String
  kd_divisi: String
  nm_divisi: String
  flag_paramedis: String
  nm_subdivisi: String
  kd_subdivisi: String
  aktifbekerja: String
  tgl_masuk: String
  sts_kerja: String
  sts_hutang: String
  sts_prg: String
  id_prg: String
  pass_prg: String
  kd_ruang: String
  nm_ruang: String
  level: String
}

type Query {
  karyawanByNIK(
    nik: String!
  ): Karyawan
}

schema {
  query: Query
}
`;