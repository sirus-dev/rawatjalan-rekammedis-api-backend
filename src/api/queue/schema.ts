// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const queueType = `
type Queue {
  id_antrian: String
  kode_unit: String
  poli: String
  nodok: String
  nama_dok: String
  no_registrasi: String
  no_medrec: String
  nama_pasien: String
  no_antrian: Int
  jaminan: String
  umur: Int
  tgl_lahir: String
  kelamin: String
  status_pasien: String
  kaji: String
  tgl_antri: String
  flag: Int
}

type QueueExchange {
  no_medrec: String
  nodok: String
  flag: Int
}

type Query {
  queueAll: [Queue]
  queue(
    poli: String!
    nama_dok: String!
    flag: Int!
  ): [Queue]
  queueToday(
    poli: String!
    nama_dok: String!
    flag: Int!
  ): [Queue]
  queueById(
    no_medrec: String!
  ): Queue
  queueAllToday(
    nodok: String!
  ): [Queue]
  groupByPoli: [Queue]
  groupByDoctor(
    poli: String
  ): [Queue]
}

type Mutation {
  addQueue(
    id_antrian: String!
    kode_unit: String!
    poli: String!
    nodok: String!
    nama_dok: String!
    no_registrasi: String!
    no_medrec: String!
    nama_pasien: String!
    no_antrian: Int!
    jaminan: String
    umur: Int!
    tgl_lahir: String
    kelamin: String!
    status_pasien: String
    kaji: String
  ): Queue
  setQueueStatus(
    no_medrec: String!
    status_pasien: String!
    flag: Int!
  ): Queue
  setReviewStatus(
    no_medrec: String!
    kaji: String!
  ): Queue
  setQueueFlag(
    no_medrec: String!
    flag: Int!
  ): Queue
  deleteQueue(
    no_medrec: String!
  ): Queue
}

type Subscription {
  queueDataExchange(
    nodok: String!
    flag: Int!
  ): QueueExchange
}

schema {
  query: Query
  mutation: Mutation
  subscription: Subscription
}
`;