import { Queue, RM } from '../connectors';
import { PubSub, withFilter } from 'graphql-subscriptions';

export const pubsub = new PubSub();
const QUEUE_EXCHANGE_TOPIC = 'queue_exchange';

// create the resolve functions for the available GraphQL queries
export const queueResolver = {

    Query: {
        /**
         * queueAll
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        queueAll(_: any, args: any) {
            return Queue.findAll({where: args});
        },
        /**
         * queue
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        queue(_: any, args: any) {
            return Queue.findAll({
                where: args
            });
        },
        /**
         * queueToday
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        queueToday(_: any, args: any) {
            var today = new Date().toLocaleDateString();
            return Queue.findAll({
                where: { tgl_antri: (today), poli: (args.poli), nama_dok: (args.nama_dok), flag: (args.flag) }
            });
        },
        /**
         * queueById
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        queueById(_: any, args: any) {
            return Queue.findOne({
                where: {no_medrec: (args.no_medrec)}
            });
        },
        /**
         * queueAllToday
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        queueAllToday(_: any, args: any) {
            var today = new Date().toLocaleDateString();
            return Queue.findAll({
                where: {tgl_antri: (today), nodok: (args.nodok)}
            });
        },
        /**
         * groupByPoli
         *
         * @returns
         */
        async groupByPoli() {
            return  await Queue.findAll({
                group: ['poli']
            });
        },
        /**
         * groupByDoctor
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async groupByDoctor(_: any, args: any) {
            return  await Queue.findAll({
                where: args,
                group: ['nama_dok']
            });
        },
    },

    Mutation: {
        /**
         * addQueue
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addQueue(_: any, args: any) {
            RM.findOrCreate({where: {
                no_medrec: (args.no_medrec),
                pasien: (args.no_medrec),
                kajian: (args.no_medrec),
                periksa: (args.no_medrec)
            }});
            pubsub.publish(QUEUE_EXCHANGE_TOPIC, { queueDataExchange: { no_medrec: args.no_medrec, nodok: args.nodok, flag: 0 }});
            var date = new Date().toLocaleDateString();
            return Queue.create({
                id_antrian: args.id_antrian,
                kode_unit: args.kode_unit,
                poli: args.poli,
                nodok: args.nodok,
                nama_dok: args.nama_dok,
                no_registrasi: args.no_registrasi,
                no_medrec: args.no_medrec,
                nama_pasien: args.nama_pasien,
                no_antrian: args.no_antrian,
                jaminan: args.jaminan,
                umur: args.umur,
                tgl_lahir: args.tgl_lahir,
                kelamin: args.kelamin,
                status_pasien: args.status_pasien,
                kaji: args.kaji,
                tgl_antri: date,
                flag: 0
            });
        },
        /**
         * setQueueStatus
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async setQueueStatus(_: any, args: any) {
            await Queue.update({ status_pasien: (args.status_pasien), flag: (args.flag) }, {where: {no_medrec: (args.no_medrec)}});
            return await Queue.findOne({where: {no_medrec: (args.no_medrec)}});
        },
        /**
         * setReviewStatus
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async setReviewStatus(_: any, args: any) {
            await Queue.update({ kaji: (args.kaji) }, {where: {no_medrec: (args.no_medrec)}});
            return await Queue.findOne({where: {no_medrec: (args.no_medrec)}});
        },
        /**
         * setQueueFlag
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async setQueueFlag(_: any, args: any) {
            await Queue.update({ flag: (args.flag) }, {where: {no_medrec: (args.no_medrec)}});
            var data  = await Queue.findOne({where: {no_medrec: (args.no_medrec)}});
            pubsub.publish(QUEUE_EXCHANGE_TOPIC, { queueDataExchange: { no_medrec: args.no_medrec, nodok: data.nodok, flag: args.flag }});
            return data;
        },
        /**
         * deleteQueue
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        deleteQueue(_: any, args: any) {
            return Queue.destroy({where: {no_medrec: args.no_medrec}});
        },
    },

    Subscription: {
        queueDataExchange: {
            subscribe: withFilter(
                () => pubsub.asyncIterator(QUEUE_EXCHANGE_TOPIC),
                (payload, variables) => {
                    return payload.queueDataExchange.flag === variables.flag && payload.queueDataExchange.nodok === variables.nodok;
                }
            )
        },
    },
};