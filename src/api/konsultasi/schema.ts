// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const ConsultationType = `
type Consultation {
  no_konsultasi: String
  no_medrec: String
  nodok: String
  id_ct: String
  alasan: String
  nodok_tujuan: String
  id_ct_jawaban: String
  jawaban: String
}

type Query {
  consultationAll: [Consultation]
  consultationPagination(
    offset: Int,
    limit: Int
  ): [Consultation]
  consultationByNodok(
    nodok: String
  ): [Consultation]
  consultationByNodok1(
    no_medrec: String
    nodok: String
  ): [Consultation]
  consultationByTujuan(
    nodok_tujuan: String
  ): [Consultation]
  consultationByTujuan1(
    nodok_tujuan: String
  ): [Consultation]
}

type Mutation {
  addConsultation(
    no_konsultasi: String
    no_medrec: String
    nodok: String
    id_ct: String
    alasan: String
    nodok_tujuan: String
  ): Consultation
  updateConsultationAnswer(
    no_konsultasi: String
    id_ct_jawaban: String
    jawaban: String
  ): Consultation
}

schema {
  query: Query
}
`;