import { Konsultasi } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const consultationResolver = {

    Query: {
        /**
         * consultationAll
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        consultationAll(_: any, args: any) {
            return Konsultasi.findAll({where: args});
        },
        /**
         * consultationPagination
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        consultationPagination(_: any, args: any) {
            return Konsultasi.findAll({where: args}, {offset: args.offset, limit: args.limit});
        },
        /**
         * consultationByNodok
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        consultationByNodok(_: any, args: any) {
            return Konsultasi.findAll({where: args});
        },
        /**
         * consultationByNodok1
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        consultationByNodok1(_: any, args: any) {
            return Konsultasi.findAll({where: args, order: [['createdAt', 'DESC']], limit: 1});
        },
        /**
         * consultationByTujuan
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        consultationByTujuan(_: any, args: any) {
            return Konsultasi.findAll({where: args});
        },
        /**
         * consultationByTujuan1
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        consultationByTujuan1(_: any, args: any) {
            return Konsultasi.findAll({where: args, order: [['createdAt', 'DESC']], limit: 1});
        }
    },

    Mutation: {
        /**
         * addConsultation
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addConsultation(_: any, args: any) {
            return Konsultasi.create(args);
        },
        /**
         * updateConsultationAnswer
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        updateConsultationAnswer(_: any, args: any) {
            return Konsultasi.update({
                id_ct_jawaban: (args.id_ct_jawaban),
                jawaban: (args.jawaban)
            }, {where: {no_konsultasi: (args.no_konsultasi)}});
        }
    }

};