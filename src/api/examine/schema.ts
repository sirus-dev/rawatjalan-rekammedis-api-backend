// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const examinationType = `
type Examine {
  no_periksa: String
  kode_unit: String
  poli: String
  tgl: String
  nodok: String
  nama_dok: String
  specialis: String
  no_registrasi: String
  no_medrec: String
  nama_pasien: String
  diagnosa: String
  diagnosa_stroke: String
  no_antrian: Int
  kasus: String
  jaminan: String
  kunjungan: String
  rujuk: String
  umur: Int
  tgl_lahir: String
  kelamin: String
  status_pasien: String
  kaji: String
  flag: Int
}

type ExamineExchange{
  nodok: String
  no_medrec: String
  flag: Int
}

type ExamineJoin {
  no_periksa: String
  kode_unit: String
  poli: String
  tgl: String
  nodok: [Doctorr]
  nama_dok: String
  specialis: String
  no_registrasi: String
  no_medrec: [Patientr]
  nama_pasien: String
  diagnosa: String
  diagnosa_stroke: String
  no_antrian: Int
  kasus: String
  jaminan: String
  kunjungan: String
  rujuk: String
  umur: Int
  tgl_lahir: String
  kelamin: String
  status_pasien: String
  kaji: String
  flag: Int
}

type Query {
  examine: [Examine]
  examineById(
    no_medrec: String!
  ): Examine
  examineByNodok(
    nodok: String!
  ): [Examine]
  examineLast(
    nodok: String!
  ): Examine
  examineLast1(
    nodok: String!
  ): Examine
  examineJoinById(
    no_medrec: String!
  ): ExamineJoin
  examineJoinByDate(
    tgl: String!
  ): [ExamineJoin]
  examineAll(
    offset: Int
    limit: Int
  ): [ExamineJoin]
  examineAllByNodok(
    offset: Int
    limit: Int
  ): [ExamineJoin]
  examineJoinByDateRange(
    startDate: String!
    endDate: String!
  ): [ExamineJoin]
  examineJoinByVisit(
    kunjungan: String
    offset: Int
    limit: Int
  ): [ExamineJoin]
  examineJoinByNo(
    no_periksa: String
  ): ExamineJoin
}

type Mutation {
  migrateQueueIntoExamine(
    no_medrec: String
  ): Examine
  addExamination(
    no_periksa: String!
    kode_unit: String!
    poli: String!
    tgl: String!
    nodok: String!
    nama_dok: String
    specialis: String
    no_registrasi: String!
    no_medrec: String!
    nama_pasien: String
    diagnosa: String
    diagnosa_stroke: String
    no_antrian: Int
    kasus: String
    jaminan: String
    kunjungan: String
    rujuk: String
    umur: Int
    tgl_lahir: String
    kelamin: String
    status_pasien: String
    kaji: String
    flag: Int
  ): Examine
  deleteExamination(
    no_medrec: String
  ): Examine
  setExaminationFlag(
    no_medrec: String
    flag: Int
  ): Examine
  updateDiagnosisExamination(
    no_periksa: String
    diagnosa: String
    diagnosa_stroke: String
    kasus: String
  ): Examine
}

type Subscription {
  examineDataExchange(
    nodok: String!
    flag: Int!
  ): ExamineExchange
}

schema {
  query: Query
  mutation: Mutation
  subscription: Subscription
}
`;