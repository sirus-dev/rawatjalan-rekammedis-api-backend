import joinMonster from 'join-monster';
import { Examine, Queue, db } from '../connectors';
import { PubSub, withFilter } from 'graphql-subscriptions';

export const pubsub = new PubSub();
const EXAMINE_EXCHANGE_TOPIC = 'examine_exchange';

// create the resolve functions for the available GraphQL queries
export const examinationResolver = {

    Query: {
        /**
         * examine
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async examine(_: any, args: any) {
            return await Examine.findAll({
                where: args
            });
        },
        /**
         * examineById
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async examineById(_: any, args: any) {
            return await Examine.findOne({
                where: {no_medrec: (args.no_medrec)}
            });
        },
        /**
         * examineByNodok
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async examineByNodok(_: any, args: any) {
            return await Examine.findAll({
                where: {nodok: (args.nodok)}
            });
        },
        /**
         * examineLast
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async examineLast(_: any, args: any) {
            return Examine.findOne({
                where: { flag: 0, nodok: (args.nodok) }
            , order: [['createdAt', 'DESC']], limit: 1 }
            );
        },
        /**
         * examineLast1
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async examineLast1(_: any, args: any) {
            return Examine.findOne({
                where: { flag: 1, nodok: (args.nodok) }
            , order: [['createdAt', 'DESC']], limit: 1 }
            );
        },
        /**
         * examineJoinById
         *
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        examineJoinById(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
                return db.query(sql)
                .spread((results, metadata) => results);
             }, { dialect: 'mysql' });
        },
        /**
         * examineJoinByDate
         *
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        examineJoinByDate(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
                return db.query(sql)
                .spread((results, metadata) => results);
             }, { dialect: 'mysql' });
        },
        /**
         * examineAll
         *
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        examineAll(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
                let query = `${sql} LIMIT ${args.offset}, ${args.limit}`;
                return db.query(query)
                .spread((results, metadata) => results);
            }, { dialect: 'mysql' });
        },
        /**
         * examineAllByNodok
         *
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        examineAllByNodok(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
                let query = `${sql} GROUP BY examineAll.nodok LIMIT ${args.offset}, ${args.limit}`;
                return db.query(query)
                .spread((results, metadata) => results);
            }, { dialect: 'mysql' });
        },
        /**
         * examineJoinByDateRange
         *
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        examineJoinByDateRange(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
                return db.query(sql)
                .spread((results, metadata) => results);
            }, { dialect: 'mysql' });
        },
        /**
         * examineJoinByVisit
         *
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        examineJoinByVisit(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
                let query = `${sql} LIMIT ${args.offset}, ${args.limit}`;
                return db.query(query)
                .spread((results, metadata) => results);
             }, { dialect: 'mysql' });
        },
        /**
         * examineJoinByNo
         *
         * @param {*} parent
         * @param {*} args
         * @param {*} ctx
         * @param {*} resolveInfo
         * @returns
         */
        examineJoinByNo(parent: any, args: any, ctx: any, resolveInfo: any) {
            return joinMonster(resolveInfo, ctx, sql => {
                return db.query(sql)
                .spread((result, metadata) => result);
            }, { dialect: 'mysql' });
        },
    },


    Mutation: {
        /**
         * migrateQueueIntoExamine
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async migrateQueueIntoExamine(_: any, args: any) {
            var ambil = await Queue.findOne({
                where: {no_medrec: (args.no_medrec)}
            });
            pubsub.publish(EXAMINE_EXCHANGE_TOPIC, { examineDataExchange: { no_medrec: ambil.no_medrec, nodok: ambil.nodok, flag: 0 } });
            return Examine.create({
                no_periksa: (ambil.id_antrian),
                kode_unit: (ambil.kode_unit),
                poli: (ambil.poli),
                nodok: (ambil.nodok),
                nama_dok: (ambil.nama_dok),
                no_registrasi: (ambil.no_registrasi),
                no_medrec: (ambil.no_medrec),
                nama_pasien: (ambil.nama_pasien),
                no_antrian: (ambil.no_antrian),
                jaminan: (ambil.jaminan),
                umur: (ambil.umur),
                tgl_lahir: (ambil.tgl_lahir),
                kelamin: (ambil.kelamin),
                status_pasien: (ambil.status_pasien),
                kaji: (ambil.kaji),
                flag: 0,
            });
        },
        /**
         * addExamination
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addExamination(_: any, args: any) {
            pubsub.publish(EXAMINE_EXCHANGE_TOPIC, { examineDataExchange: { no_medrec: args.no_medrec, nodok: args.nodok, flag: 0 } });
            return Examine.create({
                no_periksa: (args.no_periksa),
                kode_unit: (args.kode_unit),
                poli: (args.poli),
                tgl: (args.tgl),
                nodok: (args.nodok),
                nama_dok: (args.nama_dok),
                specialis: (args.specialis),
                no_registrasi: (args.no_registrasi),
                no_medrec: (args.no_medrec),
                nama_pasien: (args.nama_pasien),
                diagnosa: (args.diagnosa),
                no_antrian: (args.no_antrian),
                kasus: (args.kasus),
                jaminan: (args.jaminan),
                kunjungan: (args.kunjungan),
                rujuk: (args.rujuk),
                umur: (args.umur),
                tgl_lahir: (args.tgl_lahir),
                kelamin: (args.kelamin),
                status_pasien: (args.status_pasien),
                kaji: (args.kaji),
                flag: 0,
            });
        },
        /**
         * deleteExamination
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        deleteExamination(_: any, args: any) {
            return Examine.destroy({where: args});
        },
        /**
         * setExaminationFlag
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async setExaminationFlag(_: any, args: any) {
            await Examine.update({ flag: (args.flag) }, {where: {no_medrec: (args.no_medrec)}});
            var data = await Examine.findOne({where: {no_medrec: (args.no_medrec)}});
            console.log(data.nodok);
            pubsub.publish(EXAMINE_EXCHANGE_TOPIC, { examineDataExchange: { no_medrec: args.no_medrec, nodok: data.nodok, flag: args.flag } });
            return data;
        },
        /**
         * updateDiagnosisExamination
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async updateDiagnosisExamination(_: any, args: any) {
            await Examine.update({ diagnosa: (args.diagnosa),
                diagnosa_stroke: (args.diagnosa_stroke),
                kasus: (args.kasus) },
                {where: {no_periksa: (args.no_periksa)}});
            return await Examine.findOne({where: {no_periksa: (args.no_periksa)}});
        },
    },

    Subscription: {
        examineDataExchange: {
            subscribe: withFilter(
                () => pubsub.asyncIterator(EXAMINE_EXCHANGE_TOPIC),
                (payload, variables) => {
                    return payload.examineDataExchange.nodok === variables.nodok && payload.examineDataExchange.flag === variables.flag;
                }
            )
        },
    },
};