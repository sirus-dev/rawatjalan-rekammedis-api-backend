// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const MasterRadiologiType = `
type MasterRadiologi {
  id_radiologi: Int
  nama_pemeriksaan: String
  kelompok: String
}

type Query {
  masterRadiologiAll: [MasterRadiologi]
}

schema {
  query: Query
}
`;