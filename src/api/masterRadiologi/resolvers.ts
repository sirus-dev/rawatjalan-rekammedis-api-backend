import { MasterRadiologi } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const MasterRadiologiResolver = {

    Query: {
        /**
         * masterRadiologiAll
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        masterRadiologiAll(_: any, args: any) {
            return MasterRadiologi.findAll({where: args});
        }
    },
};