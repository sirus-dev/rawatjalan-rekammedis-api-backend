import { Tb001 } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const tb001Resolver = {

    Query: {
        /**
         * tb001
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        tb001(_: any, args: any) {
            return Tb001.findAll({where: args});
        },
        /**
         * tb001Pagination
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        tb001Pagination(_: any, args: any) {
            return Tb001.findAll({where: {
                nama: {$like: '%' + args.nama + '%'}}, offset: args.offset, limit: args.limit});
        },
        /**
         * tb001ByName
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        tb001ByName(_: any, args: any) {
            return Tb001.findAll({where: args});
        },
    },
};