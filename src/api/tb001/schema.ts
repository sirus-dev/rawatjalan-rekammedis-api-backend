// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const tb001Type = `
type Tb001 {
  kdbr: String
  jamsostek: String
  nama: String
  kdsp: String
  status: String
  satuan: String
  hda: Float
  hda_farm: Float
  harga: Float
  stk_farm: Float
  stk_apot: Int
  iron_farm: Float
  iron_apot: Float
  pabrik: String
  kontak: String
  hp: String
  jenis: String
  generik: String
  satuanbeli: String
  formularium: String
  hdabeli: Int
  kdgol: String
  kdterapi: String
  kdgenerik: String
  kdprinsip: String
  kdsatuan1: String
  kdsatuan2: String
  kd_dokter: String
  flag: String
  user_id: String
  tanggal: String
  flag_delete: String
  kdsp2: String
  kdsp3: String
  s_apotek: String
  s_farmasi: String
  discon: Float
  discoff: Float
  cn: Float
  flag_disc: Int
  jamsostek2: String
  discoffrp: Int
  hrg_nett: Int
  user_input: String
  tgl_input: String
  jam_input: String
  flag_edit: String
  konversi: Int
  flag_subsidi: String
  flag_khusus: String
  jenis2: Int
  flag_khusus2: String
  hdj: Int
  selisih: Int
  aktif: String
  standar: String
  kd_jenis: String
  nm_jenis: String
  prbkronis: Int
}

type Query {
  tb001: [Tb001]
  tb001Pagination(
    nama: String,
    offset: Int,
    limit: Int
  ): [Tb001]
  tb001ByName(
    nama: String
  ): [Tb001]
}

schema {
  query: Query
}
`;