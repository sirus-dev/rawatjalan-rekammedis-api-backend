// Creating adapter for join monster to interact with GraphQL Apollo
export const adapter = (
{
    Query: {
        fields: {
            reportById: {
                where: (table, args) => `${table}.no_lap = ${args.no_lap}`
            },
            medRec: {
                where: (table, args) => `${table}.no_medrec = ${args.no_medrec}`
            },
            examineJoinById: {
                where: (table, args) => `${table}.no_medrec = ${args.no_medrec}`
            },
            examineJoinByVisit: {
                where: (table, args) => `${table}.kunjungan = '${args.kunjungan}'`
            },
            examineJoinByDateRange: {
                where: (table, args) => `${table}.tgl >= '${args.startDate}' AND ${table}.tgl <= '${args.endDate}'`
            },
            examineJoinByNo: {
                where: (table, args) => `${table}.no_periksa = ${args.no_periksa}`
            },
        }
    },
    MedRec: {
        sqlTable: 'rm_pasien',
        uniqueKey: 'no_medrec',
        fields: {
            no_medrec: { sqlColumn: 'no_medrec' },
            pasien: { sqlJoin: (medrecTable, patientrTable) => `${medrecTable}.pasien = ${patientrTable}.no_medrec` },
            kajian: { sqlJoin: (medrecTable, mrreviewTable) =>  `${medrecTable}.kajian = ${mrreviewTable}.no_medrec`},
            periksa: { sqlJoin: (medrecTable, mrexamineTable) => `${medrecTable}.periksa = ${mrexamineTable}.no_medrec`}
        }
    },
    MRExamine: {
        sqlTable: 'logperiksa',
        uniqueKey: 'no_periksa',
        fields: {
            no_periksa: { sqlColumn: 'no_periksa' },
            kodeUnit: { sqlColumn: 'kode_unit' },
            poli: { sqlColumn: 'poli' },
            tgl: { sqlColumn: 'tgl' },
            nodok: { sqlColumn: 'nodok' },
            nama_dok: { sqlColumn: 'nama_dok' },
            specialis: { sqlColumn: 'specialis' },
            no_registrasi: { sqlColumn: 'no_registrasi' },
            no_medrec: { sqlColumn: 'no_medrec' },
            nama_pasien: { sqlColumn: 'nama_pasien' },
            diagnosa: { sqlColumn: 'diagnosa' },
            no_antrian: { sqlColumn: 'no_antrian' },
            kasus: { sqlColumn: 'kasus' },
            jaminan: { sqlColumn: 'jaminan' },
            kunjungan: { sqlColumn: 'kunjungan' },
            rujuk: { sqlColumn: 'rujuk' },
            umur: { sqlColumn: 'umur' },
            tgl_lahir: { sqlColumn: 'tgl_lahir' },
            kelamin: { sqlColumn: 'kelamin' },
            status_pasien: { sqlColumn: 'status_pasien' },
            kaji: { sqlColumn: 'kaji' },
            flag: { sqlColumn: 'flag' }
        }
    },
    ExamineJoin: {
        sqlTable: 'logperiksa',
        uniqueKey: 'no_periksa',
        fields: {
            no_periksa: { sqlColumn: 'no_periksa' },
            kode_unit: { sqlColumn: 'kode_unit' },
            poli: { sqlColumn: 'poli' },
            tgl: { sqlColumn: 'tgl' },
            nodok: { sqlJoin: (examinejoinTable, doctorrTable) => `${examinejoinTable}.nodok = ${doctorrTable}.nodok` },
            nama_dok: { sqlColumn: 'nama_dok' },
            specialis: { sqlColumn: 'specialis' },
            no_registrasi: { sqlColumn: 'no_registrasi' },
            no_medrec: { sqlJoin: (examinejoinTable, patientrTable) => `${examinejoinTable}.no_medrec = ${patientrTable}.no_medrec` },
            nama_pasien: { sqlColumn: 'nama_pasien' },
            diagnosa: { sqlColumn: 'diagnosa' },
            no_antrian: { sqlColumn: 'no_antrian' },
            kasus: { sqlColumn: 'kasus' },
            jaminan: { sqlColumn: 'jaminan' },
            kunjungan: { sqlColumn: 'kunjungan' },
            rujuk: { sqlColumn: 'rujuk' },
            umur: { sqlColumn: 'umur' },
            tgl_lahir: { sqlColumn: 'tgl_lahir' },
            kelamin: { sqlColumn: 'kelamin' },
            status_pasien: { sqlColumn: 'status_pasien' },
            kaji: { sqlColumn: 'kaji' },
            flag: { sqlColumn: 'flag' }
        }
    },
    MRReview: {
        sqlTable: 'kaji',
        uniqueKey: 'no_kaji',
        fields: {
            no_kaji: { sqlColumn: 'no_kaji' },
            no_medrec: { sqlColumn: 'no_medrec' },
            keluhan: { sqlColumn: 'keluhan' },
            resikoA1: { sqlColumn: 'resikoA1' },
            resikoA2: { sqlColumn: 'resikoA2' },
            resikoB1: { sqlColumn: 'resikoB1' },
            resikoHasil1: { sqlColumn: 'resikoHasil1' },
            resikoHasil2: { sqlColumn: 'resikoHasil2' },
            resikoHasil3: { sqlColumn: 'resikoHasil3' },
            resikoTindakan1action: { sqlColumn: 'resikoTindakan1action' },
            resikoTindakan2action: { sqlColumn: 'resikoTindakan2action' },
            resikoTindakan3action: { sqlColumn: 'resikoTindakan3action' },
            resikoTindakan1: { sqlColumn: 'resikoTindakan1' },
            resikoTindakan2: { sqlColumn: 'resikoTindakan2' },
            resikoTindakan3: { sqlColumn: 'resikoTindakan3' },
            td: { sqlColumn: 'td' },
            td1: { sqlColumn: 'td1'},
            td2: { sqlColumn: 'td2'},
            nadi: { sqlColumn: 'nadi' },
            nadi1: { sqlColumn: 'nadi1' },
            nadi2: { sqlColumn: 'nadi2' },
            pernafasan: { sqlColumn: 'pernafasan' },
            pernafasan1: { sqlColumn: 'pernafasan1' },
            pernafasan2: { sqlColumn: 'pernafasan2' },
            suhu: { sqlColumn: 'suhu' },
            tb: { sqlColumn: 'tb' },
            bb: { sqlColumn: 'bb' },
            nyeri: { sqlColumn: 'nyeri' },
            resiko_jatuh_a: { sqlColumn: 'resiko_jatuh_a' },
            resiko_jatuh_b: { sqlColumn: 'resiko_jatuh_b' },
            gizi1: { sqlColumn: 'gizi1' },
            gizi2: { sqlColumn: 'gizi2' },
            gizi3: { sqlColumn: 'gizi3' },
            gizi3_lainnya: { sqlColumn: 'gizi3_lainnya'},
            status_fungsional: { sqlColumn: 'status_fungsional' },
            status_fungsional_ket: { sqlColumn: 'status_fungsional_ket' },
            anamnesa_keluhan: { sqlColumn: 'anamnesa_keluhan' },
            anamnesa_riwayat_penyakit: { sqlColumn: 'anamnesa_riwayat_penyakit' },
            fisik_kepala: { sqlColumn: 'fisik_kepala' },
            fisik_leher: {  sqlColumn: 'fisik_leher' },
            fisik_mulut: { sqlColumn: 'fisik_mulut' },
            fisik_paru: { sqlColumn: 'fisik_paru' },
            fisik_genitalia: { sqlColumn: 'fisik_genitalia' },
            fisik_mata: { sqlColumn: 'fisik_mata' },
            fisik_tht: { sqlColumn: 'fisik_tht' },
            fisik_jantung: { sqlColumn: 'fisik_jantung' },
            fisik_abdomen: { sqlColumn: 'fisik_abdomen' },
            fisik_ekstremitas: { sqlColumn: 'fisik_ekstremitas' },
            status_gizi: { sqlColumn: 'status_gizi' },
            pemeriksaan_gigi: { sqlColumn: 'pemeriksaan_gigi' },
            pemeriksaan_gigi_ket: { sqlColumn: 'pemeriksaan_gigi_ket' },
            status_lokalis: { sqlColumn: 'status_lokalis' },
            pemeriksaan_penunjang: { sqlColumn: 'pemeriksaan_penunjang' },
            diagnosis: { sqlColumn: 'diagnosis' },
            rencana: { sqlColumn: 'rencana' },
            dirujuk: { sqlColumn: 'dirujuk' },
            dirujuk_lainnya: { sqlColumn: 'dirujuk_lainnya' },
            flagKaji: { sqlColumn: 'flagKaji' }
        }
    },
    Report: {
        sqlTable: 'laporan',
        uniqueKey: 'no_lap',
        fields: {
            no_lap: { sqlColumn: 'no_lap'},
            tgl: { sqlColumn: 'tgl' },
            no_medrec: { sqlJoin: (reportTable, patientrTable) => `${reportTable}.no_medrec = ${patientrTable}.no_medrec` },
            nodok: { sqlJoin: (reportTable, doctorrTable) => `${reportTable}.nodok = ${doctorrTable}.nodok` },
            pembayaran: { sqlColumn: 'pembayaran'},
            diagnosa: { sqlColumn: 'diagnosa' },
            kasus: { sqlColumn: 'kasus' },
            kunjungan: { sqlColumn: 'kunjungan' },
            rujuk: { sqlColumn: 'rujuk' },
            rawat_inap: { sqlColumn: 'rawat_inap'},
            alamat: { sqlColumn: 'alamat' }
        }
    },
    Patientr: {
        sqlTable: 'dtpasien',
        uniqueKey: 'no_medrec',
        fields: {
          no_medrec: { sqlColumn: 'no_medrec' },
          name: { sqlColumn: 'nama' },
          nickName: { sqlColumn: 'nama_panggil' },
          birthday: {
            sqlDeps: ['tgl_lahir', 'tgl', 'bulan', 'tahun'],
            resolve: dtpasien => `${dtpasien.tgl_lahir} ${dtpasien.tgl} ${dtpasien.bulan} ${dtpasien.thn}`
          },
          gender: { sqlColumn: 'kelamin' },
          occupation: { sqlColumn: 'pekerjaan' },
          company: { sqlColumn: 'perusahaan' },
          religion: { sqlColumn: 'agama' },
          address: {
            sqlDeps: [
              'jln1', 'no', 'rt', 'rw', 'desa',
              'kecamatan', 'kota',
            ],
            resolve: dtpasien => `${dtpasien.jln1} ${'No. ' + dtpasien.no} ${dtpasien.rt} ` +
                `${'/' + dtpasien.rw} ${'Desa. ' + dtpasien.desa} ${'Kec. ' + dtpasien.kecamatan + ','} ` +
                `${dtpasien.kota}`
          },
          contact: {
            sqlDeps: [
              'telp', 'telp2'
            ],
            resolve: dtpasien => `${dtpasien.telp} ${dtpasien.telp2}`
          },
          insurance: { sqlColumn: 'nama_penjamin' }
        }
    },
    Doctorr: {
        sqlTable: 'dtdokter',
        uniqueKey: 'nodok',
        fields: {
          name: { sqlColumn: 'nama_dok' },
          nodok: { sqlColumn: 'nodok' },
          nickName: { sqlColumn: 'nm_panggil' },
          gender: { sqlColumn: 'jenkel' },
          birthday: {
            sqlDeps: ['tgl', 'bln', 'thn'],
            resolve: dtdokter => `${dtdokter.tgl} ${dtdokter.bln} ${dtdokter.thn}`
          },
          occupation: { sqlColumn: 'pekerjaan' },
          company: { sqlColumn: 'pt' },
          religion: { sqlColumn: 'agama' },
          address: {
            sqlDeps: [
              'kp', 'jln', 'no', 'rt', 'rw', 'desa', 'kec',
              'kota'
            ],
            resolve: dtdokter => `${dtdokter.kp} ${dtdokter.jln} ${dtdokter.no}
            ${dtdokter.rt} ${dtdokter.rw} ${dtdokter.desa} ${dtdokter.kec} ${dtdokter.kota}`
          },
          contact: {
            sqlDeps: [
              'area', 'telp', 'areahp1', 'hp1', 'hp2',
              'areahp2', 'email'
            ],
            resolve: dtdokter => `${dtdokter.area} ${dtdokter.telp} ${dtdokter.areahp1}
            ${dtdokter.hp1} ${dtdokter.hp2} ${dtdokter.areahp2} ${dtdokter.email}`
          },
          number: { sqlColumn: 'nodok' },
          specialist: { sqlColumn: 'specialis' },
          npwp: {
            sqlDeps: ['npwp1', 'npwp2', 'npwp3', 'npwp4', 'npwp5'],
            resolve: dtdokter => `${dtdokter.npwp1} ${dtdokter.npwp2} ${dtdokter.npwp3} ${dtdokter.npwp4} ${dtdokter.npwp5}`
          }
        }
    }
}
);