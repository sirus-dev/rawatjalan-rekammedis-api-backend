import { mergeResolvers } from 'merge-graphql-schemas';
import { queueResolver } from './queue/resolvers';
import { reviewResolver } from './review/resolvers';
import { examinationResolver } from './examine/resolvers';
import { ICDXResolver } from './icdx/resolvers';
import { tb001Resolver } from './tb001/resolvers';
import { reportResolver } from './report/resolvers';
import { CTResolver } from './integratedNote/resolvers';
import { patientResolver } from './patient/resolvers';
import { medRecResolver } from './MedicalRecord/resolvers';
import { doctorResolver } from './doctor/resolvers';
import { ICD9Resolver } from './icd9/resolvers';
import { kidReviewResolver } from './kidReview/resolvers';
import { informConsentResolver } from './informConsent/resolvers';
import { consultationResolver } from './konsultasi/resolvers';
import { tipeInformResolver } from './typeInform/resolvers';
import { rujukanResolver } from './reference/resolvers';
import { karyawanResolver } from './employee/resolvers';
import { kebidananResolver } from './midwifery/resolvers';
import { labIntegrationResolver } from './labIntegration/resolvers';
import { MasterLabResolver } from './masterLab/resolvers';
import { MasterRadiologiResolver } from './masterRadiologi/resolvers';
import { formsResolver } from './forms/resolvers';

// Merge all resolver into 1 variable
const resolvers = mergeResolvers([
    formsResolver,
    queueResolver,
    reviewResolver,
    examinationResolver,
    ICDXResolver,
    tb001Resolver,
    reportResolver,
    CTResolver,
    patientResolver,
    medRecResolver,
    doctorResolver,
    ICD9Resolver,
    kidReviewResolver,
    informConsentResolver,
    consultationResolver,
    tipeInformResolver,
    rujukanResolver,
    karyawanResolver,
    kebidananResolver,
    labIntegrationResolver,
    MasterLabResolver,
    MasterRadiologiResolver,
]);

export { resolvers };