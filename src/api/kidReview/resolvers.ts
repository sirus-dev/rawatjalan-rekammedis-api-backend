import { KJAnak } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const kidReviewResolver = {

    Query: {
        /**
         * kidReview
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        kidReview(_: any, args: any) {
            return KJAnak.findAll({where: args});
        },
        /**
         * kidReviewByMedrec
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        kidReviewByMedrec(_: any, args: any) {
            return KJAnak.findOne({where: args});
        },
        /**
         * kidReviewByMedrec0
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        kidReviewByMedrec0(_: any, args: any) {
            return KJAnak.findOne({where: (args.no_medrec), flagKaji: 0});
        }
    },

    Mutation: {
        /**
         * addKidReview
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addKidReview(_: any, args: any) {
            return KJAnak.create(args);
        },
        /**
         * updateKidReview
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async updateKidReview(_: any, args: any) {
            await KJAnak.update(args, { where: {no_kaji: (args.no_kaji)} });
            return KJAnak.findOne({ where: {no_kaji: (args.no_kaji)} });
        },
        /**
         * kidReviewFlagUpdate
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async kidReviewFlagUpdate(_: any, args: any) {
            await KJAnak.update(args, { where: {no_medrec: (args.no_medrec), flagKaji: 0}});
            return KJAnak.findOne({ where: {no_kaji: (args.no_kaji)} });
        }
    }
};