import { mergeTypes } from 'merge-graphql-schemas';
import { queueType } from './queue/schema';
import { reviewType } from './review/schema';
import { examinationType } from './examine/schema';
import { ICDXType } from './icdx/schema';
import { tb001Type } from './tb001/schema';
import { reportType } from './report/schema';
import { CTType } from './integratedNote/schema';
import { patientType } from './patient/schema';
import { medRecType } from './MedicalRecord/schema';
import { doctorType } from './doctor/schema';
import { ICD9Type } from './icd9/schema';
import { kidReviewType } from './kidReview/schema';
import { InformConsentType } from './informConsent/schema';
import { ConsultationType } from './konsultasi/schema';
import { TipeInformType } from './typeInform/schema';
import { RujukanType } from './reference/schema';
import { KaryawanType } from './employee/schema';
import { MidwiferyType } from './midwifery/schema';
import { LabIntegrationType } from './labIntegration/schema';
import { MasterLabType } from './masterLab/schema';
import { MasterRadiologiType } from './masterRadiologi/schema';
import { formsType } from './forms/schema';

// Merge all types into 1 variables
const typeDefs = mergeTypes([
    formsType,
    queueType,
    reviewType,
    examinationType,
    ICDXType,
    tb001Type,
    reportType,
    CTType,
    patientType,
    medRecType,
    doctorType,
    ICD9Type,
    kidReviewType,
    InformConsentType,
    ConsultationType,
    TipeInformType,
    RujukanType,
    KaryawanType,
    MidwiferyType,
    LabIntegrationType,
    MasterLabType,
    MasterRadiologiType,
]);

export { typeDefs };