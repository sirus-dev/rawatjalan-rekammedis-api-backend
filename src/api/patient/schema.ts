// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const patientType = `
type Patient {
  no_medrec: String
  sts: String
  nama: String
  nama1: String
  nama2: String
  nama3: String
  id: String
  no_ktp: String
  nama_panggil: String
  kelamin: String
  tempat: String
  tgl_lahir: String
  tgl: String
  bulan: String
  tahun: String
  status: String
  agama: String
  pekerjaan: String
  perusahaan: String
  desa: String
  jln1: String
  no: String
  rt: String
  rw: String
  kecamatan: String
  kota: String
  telp: String
  hubungan: String
  id2: String
  no_ktp2: String
  sts2: String
  nama_penjamin: String
  nama_panggil2: String
  nama21: String
  nama22: String
  nama23: String
  pekerjaan2: String
  perusahaan2: String
  jln2: String
  desa2: String
  no2: String
  rt2: String
  rw2: String
  kecamatan2: String
  kota2: String
  telp2: String
  userid: String
  jam: String
  tgl: String
  komplek1: String
  komplek2: String
  tempat2: String
  tgl2: String
  bln2: String
  tahun2: String
  flag_cetak: String
  jnspt1: String
  jnspt2: String
}

type Query {
  patientByNorm(
    no_medrec: String
  ): Patient
  patientByName(
    nama: String
  ): [Patient]
}

schema {
  query: Query
}
`;