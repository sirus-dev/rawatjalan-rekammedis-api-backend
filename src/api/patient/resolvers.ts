import { Patient } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const patientResolver = {

    Query: {
        /**
         * patientByNorm
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        patientByNorm(_: any, args: any) {
            return Patient.findOne({where: args});
        },
        /**
         * patientByName
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        patientByName(_: any, args: any) {
            return Patient.findAll({where: {
                nama: {$like: '%' + args.nama + '%'}}});
        },
    },
};