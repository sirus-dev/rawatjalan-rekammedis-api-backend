// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const reviewType = `
type Review {
  no_kaji: String
  no_medrec: String
  nama_dok: String
  nodok: String
  nama_prw: String
  nik: String
  keluhan: String
  resikoA1: String
  resikoA2: String
  resikoB1: String
  resikoHasil1: String
  resikoHasil2: String
  resikoHasil3:String
  resikoTindakan1action: String
  resikoTindakan2action: String
  resikoTindakan3action: String
  resikoTindakan1: String
  resikoTindakan2: String
  resikoTindakan3: String
  td: String
  td1: Int
  td2: Int
  nadi: String
  nadi1: Int
  nadi2: Int
  pernafasan: String
  pernafasan1: Int
  pernafasan2: Int
  suhu: Float
  tb: Int
  bb: Int
  nyeri: String
  resiko_jatuh_a: String
  resiko_jatuh_b: String
  gizi1: String
  gizi2: String
  gizi3: String
  gizi3_lainnya: String
  status_fungsional: String
  status_fungsional_ket: String
  anamnesa_keluhan: String
  anamnesa_riwayat_penyakit: String
  fisik_kepala: String
  fisik_leher: String
  fisik_mulut: String
  fisik_paru: String
  fisik_genitalia: String
  fisik_mata: String
  fisik_tht: String
  fisik_jantung: String
  fisik_abdomen: String
  fisik_ekstremitas: String
  status_gizi: String
  pemeriksaan_gigi: String
  pemeriksaan_gigi_ket: String
  status_lokalis: String
  pemeriksaan_penunjang: String
  diagnosis: String
  rencana: String
  dirujuk: String
  dirujuk_lainnya: String
  tt_perawat: String
  tt_dokter: String
  flagKaji: Int
}

type Query {
  review: [Review]
  reviewByMedrec(
    no_medrec: String!
  ): Review
  reviewByMedrec0(
    no_medrec: String!
  ): Review
}

type Mutation {
  adultReview(
    no_kaji: String
    no_medrec: String
    nama_prw: String
    nik: String
    keluhan: String
    resikoA1: String
    resikoA2: String
    resikoB1: String
    resikoHasil1: String
    resikoHasil2: String
    resikoHasil3:String
    resikoTindakan1action: String
    resikoTindakan2action: String
    resikoTindakan3action: String
    resikoTindakan1: String
    resikoTindakan2: String
    resikoTindakan3: String
    td: String
    td1: Int
    td2: Int
    nadi: String
    nadi1: Int
    nadi2: Int
    pernafasan: String
    pernafasan1: Int
    pernafasan2: Int
    suhu: Float
    tb: Int
    bb: Int
    nyeri: String
    resiko_jatuh_a: String
    resiko_jatuh_b: String
    gizi1: String
    gizi2: String
    gizi3: String
    gizi3_lainnya: String
    status_fungsional: String
    status_fungsional_ket: String
    anamnesa_keluhan: String
    anamnesa_riwayat_penyakit: String
    fisik_kepala: String
    fisik_leher: String
    fisik_mulut: String
    fisik_paru: String
    fisik_genitalia: String
    fisik_mata: String
    fisik_tht: String
    fisik_jantung: String
    fisik_abdomen: String
    fisik_ekstremitas: String
    status_gizi: String
    pemeriksaan_gigi: String
    pemeriksaan_gigi_ket: String
    status_lokalis: String
    pemeriksaan_penunjang: String
    diagnosis: String
    rencana: String
    dirujuk: String
    dirujuk_lainnya: String
    tt_perawat: String
    tt_dokter: String
    flagKaji: Int
  ): Review
  updateReview(
    no_kaji: String
    no_medrec: String
    keluhan: String
    resikoA1: String
    resikoA2: String
    resikoB1: String
    resikoHasil1: String
    resikoHasil2: String
    resikoHasil3:String
    resikoTindakan1action: String
    resikoTindakan2action: String
    resikoTindakan3action: String
    resikoTindakan1: String
    resikoTindakan2: String
    resikoTindakan3: String
    td: String
    td1: Int
    td2: Int
    nadi: String
    nadi1: Int
    nadi2: Int
    pernafasan: String
    pernafasan1: Int
    pernafasan2: Int
    suhu: Float
    tb: Int
    bb: Int
    nyeri: String
    resiko_jatuh_a: String
    resiko_jatuh_b: String
    gizi1: String
    gizi2: String
    gizi3: String
    gizi3_lainnya: String
    status_fungsional: String
    status_fungsional_ket: String
    tt_perawat: String
    flagKaji: Int
  ): Review
  reviewFlagUpdate(
    no_medrec: String
    flagKaji: Int
  ): Review
  reviewDoctorAssessment(
    no_kaji: String
    nama_dok: String
    nodok: String
    anamnesa_keluhan: String
    anamnesa_riwayat_penyakit: String
    fisik_kepala: String
    fisik_leher: String
    fisik_mulut: String
    fisik_paru: String
    fisik_genitalia: String
    fisik_mata: String
    fisik_tht: String
    fisik_jantung: String
    fisik_abdomen: String
    fisik_ekstremitas: String
    status_gizi: String
    pemeriksaan_gigi: String
    pemeriksaan_gigi_ket: String
    status_lokalis: String
    pemeriksaan_penunjang: String
    diagnosis: String
    rencana: String
    dirujuk: String
    dirujuk_lainnya: String
    tt_dokter: String
  ): Review
}

schema {
  query: Query
  mutation: Mutation
}
`;