import { Review } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const reviewResolver = {

    Query: {
        /**
         * review
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        review(_: any, args: any) {
            return Review.findAll({where: args});
        },
        /**
         * reviewByMedrec
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        reviewByMedrec(_: any, args: any) {
            return Review.findOne({where: args});
        },
        /**
         * reviewByMedrec0
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        reviewByMedrec0(_: any, args: any) {
            return Review.findOne({where: {no_medrec: (args.no_medrec), flagKaji: 0}});
        }
    },

    Mutation: {
        /**
         * adultReview
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        adultReview(_: any, args: any) {
            return Review.create(args);
        },
        /**
         * updateReview
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async updateReview(_: any, args: any) {
            await Review.update(args, { where: {no_kaji: (args.no_kaji)} });
            return Review.findOne({ where: {no_kaji: (args.no_kaji)} });

        },
        /**
         * reviewFlagUpdate
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async reviewFlagUpdate(_: any, args: any) {
            await Review.update(args, { where: {no_medrec: (args.no_medrec)} });
            return Review.findOne({ where: {no_medrec: (args.no_medrec)} });
        },
        /**
         * reviewDoctorAssessment
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async reviewDoctorAssessment(_: any, args: any) {
            await Review.update(args, { where: {no_kaji: (args.no_kaji)} });
            return Review.findOne({ where: {no_kaji: (args.no_kaji)} });
        }
    }
};