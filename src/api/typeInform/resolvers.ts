import { TipeInform } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const tipeInformResolver = {

    Query: {
        /**
         * informType
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        informType(_: any, args: any) {
            return TipeInform.findAll({where: args});
        },
        /**
         * informTypeByName
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        informTypeByName(_: any, args: any) {
            return TipeInform.findOne({where: args});
        },
    },
};