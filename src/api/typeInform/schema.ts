// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const TipeInformType = `
type TipeInform {
  id_type_inform: String
  tipe_inform: String
  diagnosis_kerja: String
  dasar_diagnosis: String
  tindakan_dokter: String
  indikasi_tindakan: String
  tatacara: String
  tujuan: String
  risiko: String
  komplikasi: String
  prognosis: String
  alternatif: String
  lainLain: String
}

type Query {
  informType: [TipeInform]
  informTypeByName(
    tipe_inform: String
  ): TipeInform
}

schema {
  query: Query
}
`;