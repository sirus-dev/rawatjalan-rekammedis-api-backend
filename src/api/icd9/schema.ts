// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const ICD9Type = `
type ICD9 {
  kode: String
  deskripsi: String
  klasifikasi: String
  kelompok: String
}

type Query {
  icd9: [ICD9]
  icd9Pagination(
    deskripsi: String,
    offset: Int,
    limit: Int
  ): [ICD9]
  icd9ByName(
    deskripsi: String
  ): [ICD9]
}

schema {
  query: Query
}
`;