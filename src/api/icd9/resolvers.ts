import { ICD9 } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const ICD9Resolver = {

    Query: {
        /**
         * icd9
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        icd9(_: any, args: any) {
            return ICD9.findAll({where: args});
        },
        /**
         * icd9Pagination
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        icd9Pagination(_: any, args: any) {
            return ICD9.findAll({where: {
                deskripsi: {$like: '%' + args.deskripsi + '%'}}, offset: args.offset, limit: args.limit});
        },
        /**
         * icd9ByName
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        icd9ByName(_: any, args: any) {
            return ICD9.findAll({where: args});
        },
    },

};