import { CT } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const CTResolver = {

    Query: {
        /**
         * ct
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        ct(_: any, args: any) {
            return CT.findAll({where: args});
        },
        /**
         * ctById
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        ctById(_: any, args: any) {
            return CT.findOne({where: args});
        },
        /**
         * ctByMedrec
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        ctByMedrec(_: any, args: any) {
            return CT.findAll({where: args, order: [['tgl_ct', 'DESC']]});
        },
        /**
         * ctByMedrec1
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        ctByMedrec1(_: any, args: any) {
            return CT.findAll({where: args, order: [['tgl_ct', 'DESC']], limit: 1});
        },
        /**
         * ctByMedrecTgl
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        ctByMedrecTgl(_: any, args: any) {
            return CT.findAll({where: args});
        },
        /**
         *ctByDate
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        ctByDate(_: any, args: any) {
            return CT.findAll({where: args});
        },
        /**
         * ctByExamine
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        ctByExamine(_: any, args: any) {
            return CT.findAll({where: args});
        },
        /**
         * ctByNodok1
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        ctByNodok1(_: any, args: any) {
            return CT.findAll({where: args, order: [['tgl_ct', 'DESC']], limit: 1});
        },
        /**
         * checkICD10
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        checkICD10(_: any, args: any) {
            return CT.findAll({where: {no_medrec: (args.no_medrec) , a_icdx: {$like: '%' + args.a_icdx + '%'}}});
        }
    },
    Mutation: {
        /**
         * addCt
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addCt(_: any, args: any) {
            return CT.create(args);
        },
        /**
         * deleteCt
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        deleteCt(_: any, args: any) {
            return CT.destroy({where: args});
        },
        /**
         * updateCt
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        updateCt(_: any, args: any) {
            CT.update({
                s_description: args.s_description,
                s_stroke: args.s_stroke,
                o_description: args.o_description,
                o_stroke: args.o_stroke,
                a_description: args.a_description,
                a_stroke: args.a_stroke,
                a_icd9: args.a_icd9,
                a_icdx: args.a_icdx,
                p_lab_description: args.p_lab_description,
                p_lab_stroke: args.p_lab_stroke,
                p_rad_description: args.p_rad_description,
                p_rad_stroke: args.p_rad_stroke,
                p_fisio_description: args.p_fisio_description,
                p_fisio_stroke: args.p_fisio_stroke,
                p_tindakan_description: args.p_tindakan_description,
                p_tindakan_stroke: args.p_tindakan_stroke,
                p_obat_description: args.p_obat_description,
                p_obat_stroke: args.p_obat_stroke,
                p_obat_nama: args.p_obat_nama,
                p_obat_alergi: args.p_obat_alergi,
                penyakit: args.penyakit
            }, {where: { no_medrec: (args.no_medrec)}});
            return CT.findOne({where: { no_medrec: (args.no_medrec)}});
        },
        /**
         * updateCtIcd9
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        updateCtIcd9(_: any, args: any) {
            CT.update({
                a_icd9: args.a_icd9,
            }, {where: { no_medrec: (args.no_medrec)}});
            return CT.findOne({where: { no_medrec: (args.no_medrec)}});
        }
    },
};