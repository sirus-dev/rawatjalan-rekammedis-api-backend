// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const CTType = `
type CT {
  id_ct: String
  no_medrec: String
  nama_pasien: String
  nodok: String
  nama_dok: String
  no_periksa: String
  jaminan: String
  tgl_lahir: String
  umur: Int
  kelamin: String
  kelas: String
  tgl_ct: String
  s_description: String
  s_stroke: String
  o_description: String
  o_stroke: String
  a_description: String
  a_stroke: String
  a_icd9: String
  a_icdx: String
  p_lab_description: String
  p_lab_stroke: String
  p_rad_description: String
  p_rad_stroke: String
  p_fisio_description: String
  p_fisio_stroke: String
  p_tindakan_description: String
  p_tindakan_stroke: String
  p_obat_description: String
  p_obat_stroke: String
  p_obat_nama: String
  p_obat_alergi: String
  penyakit: String
}

type Query {
  ct: [CT]
  ctById(
    id_ct: String
  ): CT
  ctByMedrec(
    no_medrec: String!
  ): [CT]
  ctByMedrec1(
    no_medrec: String!
  ): [CT]
  ctByMedrecTgl(
    no_medrec: String!
    tgl_ct: String!
  ): [CT]
  ctByDate(
    no_medrec: String
    tgl_ct: String
  ): [CT]
  ctByExamine(
    no_periksa: String
  ): [CT]
  ctByNodok1(
    nodok: String
    no_medrec: String
  ): [CT]
  checkICD10(
    no_medrec: String
    a_icdx: String
  ): [CT]
}

type Mutation {
  addCt(
    id_ct: String
    no_medrec: String
    nama_pasien: String
    nodok: String
    nama_dok: String
    no_periksa: String
    jaminan: String
    tgl_lahir: String
    umur: Int
    kelamin: String
    kelas: String
    tgl_ct: String
    s_description: String
    s_stroke: String
    o_description: String
    o_stroke: String
    a_description: String
    a_stroke: String
    a_icd9: String
    a_icdx: String
    p_lab_description: String
    p_lab_stroke: String
    p_rad_description: String
    p_rad_stroke: String
    p_fisio_description: String
    p_fisio_stroke: String
    p_tindakan_description: String
    p_tindakan_stroke: String
    p_obat_description: String
    p_obat_stroke: String
    p_obat_nama: String
    p_obat_alergi: String
    penyakit: String
  ): CT
  deleteCt(
    id_ct: String
  ): CT
  updateCt(
    no_medrec: String
    s_description: String
    s_stroke: String
    o_description: String
    o_stroke: String
    a_description: String
    a_stroke: String
    a_icd9: String
    a_icdx: String
    p_lab_description: String
    p_lab_stroke: String
    p_rad_description: String
    p_rad_stroke: String
    p_fisio_description: String
    p_fisio_stroke: String
    p_tindakan_description: String
    p_tindakan_stroke: String
    p_obat_description: String
    p_obat_stroke: String
    p_obat_nama: String
    p_obat_alergi: String
    penyakit: String
  ): CT
  updateCtIcd9(
    no_medrec: String,
    a_icd9: String
  ): CT
}

schema {
  query: Query
  mutation: Mutation
}
`;