// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const LabIntegrationType = `
type LabIntegration {
    id_integrasi: String
    no_medrec: String
    indikasi: String
    diagnosis: String
    lab: String
    radiologi: String
    fisioterapi: String
    tgl_permintaan: String
}

type Query {
  labIntegrationById(
    no_medrec: String!
  ): LabIntegration
  labIntegrationAll: [LabIntegration]
  labIntegrationByDate(
    tgl_permintaan: String!
  ): [LabIntegration]
}

type Mutation {
  addLab(
    id_integrasi: String!
    no_medrec: String!
    indikasi: String!
    diagnosis: String!
    lab: String!
    tgl_permintaan: String!
  ): LabIntegration
  addRadio(
    id_integrasi: String!
    no_medrec: String!
    indikasi: String!
    diagnosis: String!
    radiologi: String!
    tgl_permintaan: String!
  ): LabIntegration
  addFisio(
    id_integrasi: String!
    no_medrec: String!
    indikasi: String!
    diagnosis: String!
    fisioterapi: String!
    tgl_permintaan: String!
  ): LabIntegration
}

schema {
  query: Query
  mutation: Mutation
}
`;