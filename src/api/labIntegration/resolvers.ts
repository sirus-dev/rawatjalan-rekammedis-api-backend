import { LabIntegration } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const labIntegrationResolver = {

    Query: {
        /**
         * labIntegrationById
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        labIntegrationById(_: any, args: any) {
            return LabIntegration.findOne({where: args});
        },
        /**
         * labIntegrationAll
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        labIntegrationAll(_: any, args: any) {
            return LabIntegration.findAll({where: args});
        },
        /**
         * labIntegrationByDate
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        labIntegrationByDate(_: any, args: any) {
            return LabIntegration.findAll({where: args});
        }
    },
    Mutation: {
        /**
         * addLab
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addLab(_: any, args: any) {
            return LabIntegration.create({args});
        },
        /**
         * addRadio
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addRadio(_: any, args: any) {
            return LabIntegration.create({args});
        },
        /**
         * addFisio
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addFisio(_: any, args: any) {
            return LabIntegration.create({args});
        },
    }
};