import { Rujukan } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const rujukanResolver = {

    Query: {
        /**
         * referenceAll
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        referenceAll(_: any, args: any) {
            return Rujukan.findAll({where: args});
        },
        /**
         * referenceById
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        referenceById(_: any, args: any) {
            return Rujukan.findOne({where: args});
        },
        /**
         * icdxByName
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        icdxByName(_: any, args: any) {
            return Rujukan.findAll({where: args});
        }
    },

    Mutation: {
        /**
         * addReference
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addReference(_: any, args: any) {
            return Rujukan.create({where: args});
        }
    },
};