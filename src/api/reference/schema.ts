// Create Types for graphQL Schema defining Queries, Mutations, and Subscriptions for each types
export const RujukanType = `
type Reference {
  id_rujukan: String
  nama_pasien: String
  no_medrec: String
  umur: String
  kelamin: String
  tgl: String
  nama_dok: String
  jaminan: String
  kelas: String
  tipe_rujukan: String
  diagnosa: String
  indikasi: String
  keterangan: String
  lab_hematologi: String
  lab_urinalisa: String
  lab_faeces: String
  lab_gula_darah: String
  lab_kimia_darah: String
  lab_berologi: String
  lab_mikrobiologi: String
  lab_lain: String
  rad_tanpa_kontras: String
  rad_kontras: String
  fisio_rindakan: String
}

type Query {
  referenceAll: [Reference]
  referenceById(
    id_rujukan: String
  ): Reference
  referenceByNorm(
    no_medrec: String
  ): [Reference]
}

type Mutation {
  addReference(
    id_rujukan: String
    nama_pasien: String
    no_medrec: String
    umur: String
    kelamin: String
    tgl: String
    nama_dok: String
    jaminan: String
    kelas: String
    tipe_rujukan: String
    diagnosa: String
    indikasi: String
    keterangan: String
    lab_hematologi: String
    lab_urinalisa: String
    lab_faeces: String
    lab_gula_darah: String
    lab_kimia_darah: String
    lab_berologi: String
    lab_mikrobiologi: String
    lab_lain: String
    rad_tanpa_kontras: String
    rad_kontras: String
    fisio_rindakan: String
  ): Reference
}

schema {
  query: Query
}
`;