export const formsType = `
type Forms {
  no_form: String
  name_form: String
  json_form: String
}

type FormsExchange {
  no_form: String
  name_form: String
}

type Query {
  formAll: [Forms]
  formById(
    no_form: String!
  ): Forms
  formByName(
    name_form: String!
  ): [Forms]
}

type Mutation {
  addForm(
    no_form: String
    name_form: String
    json_form: String!
  ): Forms
  deleteForm(
    no_form: String
  ): Forms
  updateForm(
    no_form: String
    name_form: String
    json_form: String
  ): Forms
}

type Subscription {
  formDataExchange: FormsExchange
}

schema {
  query: Query
  mutation: Mutation
  subscription: Subscription
}
`;