import { Forms } from '../connectors';
import { PubSub, withFilter } from 'graphql-subscriptions';

export const pubsub = new PubSub();
const FORM_EXCHANGE_TOPIC = 'form_exchange';


// create the resolve functions for the available GraphQL queries
export const formsResolver = {

    Query: {
        /**
         * formAll
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async formAll(_: any, args: any) {
            return await Forms.findAll({
                where: args
            });
        },
        /**
         * formById
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async formById(_: any, args: any) {
            return await Forms.findOne({
                where: {no_form: (args.no_form)}
            });
        },
        /**
         * formByName
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async formByName(_: any, args: any) {
            return await Forms.findAll({
                where: {name_form: (args.name_form)}
        });
        }
    },

    Mutation: {
        /**
         * addForm
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addForm(_: any, args: any) {
            pubsub.publish(FORM_EXCHANGE_TOPIC, { formDataExchange: { no_form: args.no_form, name_form: args.name_form }});
            return Forms.create({
                no_form: (args.no_form),
                name_form: (args.name_form),
                json_form: (args.json_form),
            });
        },
        /**
         * deleteForm
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        deleteForm(_: any, args: any) {
            pubsub.publish(FORM_EXCHANGE_TOPIC, { formDataExchange: { no_form: args.no_form, name_form: args.name_form }});
            return Forms.destroy({where: args});
        },
        /**
         * updateForm
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async updateForm(_: any, args: any) {
            await Forms.update({ name_form: (args.name_form), json_form: (args.json_form) }, {where: {no_form: (args.no_form)}});
            pubsub.publish(FORM_EXCHANGE_TOPIC, { formDataExchange: { no_form: args.no_form, name_form: args.name_form }});
            return await Forms.findOne({where: {no_form: (args.no_form)}});
        },
    },

    Subscription: {
        formDataExchange: {
            subscribe: () => pubsub.asyncIterator(FORM_EXCHANGE_TOPIC),
        },
    },
};