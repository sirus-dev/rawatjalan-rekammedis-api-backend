import { Kebidanan } from '../connectors';


// create the resolve functions for the available GraphQL queries
export const kebidananResolver = {

    Query: {
        /**
         * MWReview
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        MWReview(_: any, args: any) {
            return Kebidanan.findAll({where: args});
        },
        /**
         * MWReviewByMedrec
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        MWReviewByMedrec(_: any, args: any) {
            return Kebidanan.findOne({where: args});
        },
        /**
         * MWReviewByMedrec0
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        MWReviewByMedrec0(_: any, args: any) {
            return Kebidanan.findAll({where: {no_medrec: (args.no_medrec), flagKaji: 0}});
        }
    },
    Mutation: {
        /**
         * addMWReview
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        addMWReview(_: any, args: any) {
            return Kebidanan.create(args);
        },
        /**
         * updateMWReview
         *
         * @param {*} _
         * @param {*} args
         * @returns
         */
        async updateMWReview(_: any, args: any) {
            await Kebidanan.update(args, { where: { no_medrec: (args.no_medrec) } });
            return Kebidanan.findOne({ where: {no_kaji: (args.no_kaji)} });
        },
    }
};