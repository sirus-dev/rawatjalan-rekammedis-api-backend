import * as rc from 'rc';
import * as process from 'process';
import { join, dirname, resolve } from 'path';
import { existsSync, readFileSync, readFile } from 'fs';

/**
 * config
 *
 * @export
 * @interface Config
 */
export interface Config {
  APP_PORT: number;
  MYSQL_HOST: string;
  MYSQL_PORT: number;
  MYSQL_DB: string;
  MYSQL_USERNAME: string;
  MYSQL_PASSWORD: string;
  LOCALHOST: string;
}

// Setting default config in case .json config doesn't exist
export const configDefault: Config = {
  APP_PORT: 5000,
  MYSQL_HOST: '104.248.151.31',
  MYSQL_PORT: 14011,
  MYSQL_DB: 'sirus',
  MYSQL_USERNAME: 'root',
  MYSQL_PASSWORD: 'sukses2018',
  LOCALHOST: 'localhost'
};

/**
 * Read configuration for binary packaged build
 * configuration must be placed in JSON format with 'cbic.config.json' filename
 * File must be placed in the same directory with distributed binary file ('cbic-api')
 */
function readLocalConfig(): Config {
  const configPath = join(dirname(process.execPath), 'sirus.config.json');
  try {
    if (!existsSync(configPath)) {
      return configDefault;
    }
    const raw = readFileSync(configPath, 'utf8');
    const configLocal = JSON.parse(raw);
    return Object.assign<any, Config, Config>({}, configDefault, configLocal);
  } catch (e) {
    return configDefault;
  }
}

const config: Config = process['pkg']
  ? readLocalConfig()
  : rc<Config>('sirusapi', configDefault);

export default config;
